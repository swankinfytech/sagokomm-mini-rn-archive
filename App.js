/*!

 =========================================================
 * Material Kit React Native - v1.4.0
 =========================================================
 * Product Page: https://demos.creative-tim.com/material-kit-react-native/
 * Copyright 2019 Creative Tim (http://www.creative-tim.com)
 * Licensed under MIT (https://github.com/creativetimofficial/material-kit-react-native/blob/master/LICENSE)
 =========================================================
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/

import React from 'react';
import { Provider } from 'react-redux';
import store from 'reducers';
import GlobalFont from 'react-native-global-font';
import AppRoot from 'AppRoot';

export default class App extends React.Component {
  componentDidMount() {
    GlobalFont.applyGlobal('ArimaMadurai-Medium');
  }

  render() {
    return (
      <Provider store={store}>
        <AppRoot />
      </Provider>
    );
  }
}
