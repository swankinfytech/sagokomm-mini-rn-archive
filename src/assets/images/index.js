/* eslint global-require: 0 */

export const ICON = require('./icon.png');
export const IMG_BG1 = require('./imgbg1.jpg');
export const LOADER_BG = require('./loaderbg.png');
export const AVATAR = require('./avatar.png');
export const LOGO = require('./logo.png');
export const ONBOARDING = require('./onboarding.jpg');
export const SLANT_STRIP = require('./slant-strip.png');
export const ZOOM_LOGO = require('./zoom.png');
export const SATELLITE = require('./satellite.png');
