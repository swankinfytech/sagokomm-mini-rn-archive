const VALUES = {
  COLORS: {
    DEFAULT: '#DCDCDC',
    PRIMARY: '#9C26B0',
    LABEL: '#FE2472',
    INFO: '#00BCD4',
    ERROR: '#F44336',
    SUCCESS: '#4CAF50',
    WARNING: '#FF9800',
    MUTED: '#979797',
    INPUT: '#DCDCDC',
    ACTIVE: '#9C26B0',
    BUTTON_COLOR: '#9C26B0',
    PLACEHOLDER: '#9FA5AA',
    SWITCH_ON: '#9C26B0',
    SWITCH_OFF: '#D4D9DD',
    GRADIENT_START: '#6B24AA',
    GRADIENT_END: '#AC2688',
    PRICE_COLOR: '#EAD5FB',
    BORDER_COLOR: '#E7E7E7',
    BLOCK: '#E7E7E7',
    ICON: '#4A4A4A',
    LIGHT_GREY: '#d8d8d8',
    LOGO_ORANGE: '#c2690b',
    WHITE: '#fff',
    CUSTOM_BACKGROUND: '#ec737c',
    FACEBOOK: '#3B5998',
  },
  SIZES: {
    BLOCK_SHADOW_RADIUS: 2,
    BASE: 16,
    CARD_TITLE_SIZE: 16,
  },
};

const _shadowStyle = {
  shadowColor: 'black',
  shadowOffset: { width: 0, height: 2 },
  shadowRadius: 6,
  shadowOpacity: 0.2,
  elevation: 3,
};

const REUSABLE_STYLES = {
  SHADOW: _shadowStyle,
  CARD: {
    padding: VALUES.SIZES.BASE,
    borderRadius: 13,
    backgroundColor: VALUES.COLORS.WHITE,
    ..._shadowStyle,
  },
  SECTION_TITLE: {
    marginBottom: VALUES.SIZES.CARD_TITLE_SIZE,
    fontSize: VALUES.SIZES.CARD_TITLE_SIZE,
    fontWeight: 'bold',
  },
};

export default {
  ...VALUES,
  REUSABLE_STYLES
};
