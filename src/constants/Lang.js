import { PASSWORD_MINIMUM_LENGTH } from 'constants/Config';

export default {
  PLACEHOLDERS: {
    password: 'Enter Password',
    username: 'Enter Username',
    organizationCode: 'Enter Organization Code',
    otp: 'Enter 4-digit OTP',
    passwordConfirmation: 'Confirm Password',
    passwordReset: `Enter Password (Min ${PASSWORD_MINIMUM_LENGTH})`,
  },
  HELP_STRINGS: {

  }
};
