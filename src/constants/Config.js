export const DEV_ENV = false;
export const LOG = false;
export const BYPASS_SUBDOMAIN = false;
export const APP_VERSION = '1.7.3';

export const PROTOCOL = 'https';
export const PROD_DOMAIN = 'api-mini.sagokomm.in';
export const DEV_DOMAIN = '9b0c5c313f9f.ngrok.io';
export const DEV_MAIN_DOMAIN = '4620c788292e.ngrok.io';
export const ROOT_DOMAIN = DEV_ENV ? DEV_DOMAIN : PROD_DOMAIN;
export const MAIN_DOMAIN = DEV_ENV ? DEV_MAIN_DOMAIN : PROD_DOMAIN;
export const PASSWORD_MINIMUM_LENGTH = 6;
export const STRING_SLICING_LENGTH = 32;
export const TITLE_SLICING_LENGTH = 20;
export const DESCRIPTION_SLICING_LENGTH = 64;
export const YOUTUBE_API_KEY = 'AIzaSyC2RvDeHfik1ztpDEUfw2_N7cGllFLrIhg';
export const ZOOM_APP_KEY = 'O4X9xe8I8Yi3sSl7maHLcCujStlWkDLyRXhM';
export const ZOOM_APP_SECRET = 'JcIB5yxIbHX792TNY39NkvoVdqgmZhQKw3r9';
export const ONESIGNAL_APP_ID = 'fdba74fc-a2a6-449c-b0c2-396cc17bd7ba';

export const CONFERENCE_STATUSES = ['', 'Scheduled', 'Ongoing', 'Archived'];

export default {
  DEV_ENV,
  LOG,
  PROD_DOMAIN,
  DEV_DOMAIN,
  PROTOCOL,
  ROOT_DOMAIN,
  PASSWORD_MINIMUM_LENGTH,
  STRING_SLICING_LENGTH,
  TITLE_SLICING_LENGTH,
  DESCRIPTION_SLICING_LENGTH,
  YOUTUBE_API_KEY,
  ZOOM_APP_KEY,
  ZOOM_APP_SECRET,
  CONFERENCE_STATUSES,
  APP_VERSION,
  ONESIGNAL_APP_ID,
};
