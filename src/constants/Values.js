import { Platform, StatusBar, Dimensions } from 'react-native';
import { theme } from 'galio-framework';

const { height, width } = Dimensions.get('window');

export const StatusHeight = StatusBar.currentHeight;
export const HeaderHeight = ((theme.SIZES.BASE * 3.5) + (StatusHeight || 0));
export const iPhoneX = Platform.OS === 'ios' && (height === 812 || width === 812 || height === 896 || width === 896);

export const TOKEN_STORAGE_KEY = 'token';
export const ORGANIZATION_CODE_STORAGE_KEY = 'organization_code';
export const CONFERENCE_PROVIDERS = {
  ZOOM: 'ZOOM',
};
export const CONFERENCE_EVENTS = {
  ZOOM: {
    MEETING_STATUS_CHANGED: 'meetingStatusChanged',
  },
};
export const CONFERENCE_STATUSES = {
  ZOOM: {
    MEETING_STATUS_CONNECTING: 'MEETING_STATUS_CONNECTING',
    MEETING_STATUS_DISCONNECTING: 'MEETING_STATUS_DISCONNECTING',
    MEETING_STATUS_FAILED: 'MEETING_STATUS_FAILED',
    MEETING_STATUS_IDLE: 'MEETING_STATUS_IDLE',
    MEETING_STATUS_WAITINGFORHOST: 'MEETING_STATUS_WAITINGFORHOST',
    MEETING_STATUS_IN_WAITING_ROOM: 'MEETING_STATUS_IN_WAITING_ROOM',
    MEETING_STATUS_INMEETING: 'MEETING_STATUS_INMEETING',
    MEETING_STATUS_RECONNECTING: 'MEETING_STATUS_RECONNECTING',
  },
};

export default {
  StatusHeight,
  HeaderHeight,
  iPhoneX,
  TOKEN_STORAGE_KEY,
  ORGANIZATION_CODE_STORAGE_KEY,
  CONFERENCE_PROVIDERS,
  CONFERENCE_EVENTS,
  CONFERENCE_STATUSES,
};
