import { combineReducers, createStore, applyMiddleware } from 'redux';
import ReduxThunk from 'redux-thunk';
import AuthReducer from './AuthReducer';
import UserReducer from './UserReducer';

const reducers = combineReducers({
  Auth: AuthReducer,
  User: UserReducer,
});

export default createStore(reducers, {}, applyMiddleware(ReduxThunk));
