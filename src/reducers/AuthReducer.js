import {
  LOGIN,
  SET_ORGANIZATION_CODE,
  LOGOUT,
  SET_TOKEN,
 } from 'actions/actionTypes';

const INITIAL_STATE = {
  loggedIn: false,
  token: undefined,
  organizationCode: undefined,
  tokenError: false,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        loggedIn: true,
      };
    case SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    case SET_ORGANIZATION_CODE:
      return {
        ...state,
        organizationCode: action.payload
      };
    case LOGOUT:
      return {
        ...INITIAL_STATE,
        tokenError: action.payload && action.payload.tokenError ? action.payload.tokenError : false,
      };
    default:
      return state;
  }
};
