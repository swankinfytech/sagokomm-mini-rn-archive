import {
  LOGOUT,
  SET_USER_DATA,
} from 'actions/actionTypes';

const INITIAL_STATE = {
  user: undefined,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_USER_DATA:
      return {
        ...state,
        user: action.payload
      };
    case LOGOUT:
      return INITIAL_STATE;
    default:
      return state;
  }
};
