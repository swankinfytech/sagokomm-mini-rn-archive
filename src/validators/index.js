import validate from 'validate.js';
import { PASSWORD_MINIMUM_LENGTH } from 'constants/Config';

export const stringValidator = (value) => {
  return validate.isString(value);
};

export const requiredStringValidator = (value) => {
  return validate.isString(value) && !validate.isEmpty(value);
};

export const mobileValidator = (value) => {
  return !validate.single(value, {
    numericality: { onlyInteger: true },
    length: { is: 10 }
  });
};

export const integerValidator = (value) => {
  return !validate.single(value, { numericality: { onlyInteger: true } });
};

export const passwordValidator = (value) => {
  return validate.isString(value) && value.length >= PASSWORD_MINIMUM_LENGTH;
};

export const requiredValidator = (value) => {
  return !validate.isEmpty(value);
};

export const equalityValidator = (value1, value2) => {
  return value1 === value2;
};
