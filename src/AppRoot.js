import React from 'react';
import OneSignal from 'react-native-onesignal';
import { Platform, StatusBar } from 'react-native';
import { Block, GalioProvider } from 'galio-framework';
import { NavigationContainer } from '@react-navigation/native';
import { connect } from 'react-redux';
import AsyncStorage from 'utilities/AsyncStorageWrapper';
import { MATERIAL_THEME, CONFIG } from 'constants';
import MainNavigator from 'navigation/MainNavigator';
import AuthNavigator from 'navigation/AuthNavigator';
import { SplashScreen, ForceUpdate, AppMigrate, AppInitRetry } from 'components';
import {
  TOKEN_STORAGE_KEY,
  ORGANIZATION_CODE_STORAGE_KEY,
} from 'constants/Values';
import {
  loginAction,
  logoutAction,
  setOrganizationCodeAction,
  setTokenAction,
} from 'actions/AuthActions';
import { setUserDataAction } from 'actions/UserActions';
import { refreshTokenApi } from 'api/AuthApi';
import { getVersionApi, getMigrationStatusApi } from 'api/VersionApi';
import { initializeZoomBridge } from 'utilities/ConferenceHelpers';
import { checkVersionObsolete } from 'utilities/Helpers';

/**
 * TODO check iOS statusbar behaviour here.
 */

class AppRoot extends React.Component {
  constructor(properties) {
    super(properties);
    OneSignal.init(CONFIG.ONESIGNAL_APP_ID);
    OneSignal.inFocusDisplaying(2);
  }

  state = {
    splashScreenVisible: true,
    minAppVersion: '1.0.0',
    forceUpdate: false,
    migrateApp: false,
    androidLink: '',
    iosLink: '',
    retryScreenVisible: false,
    tokenExisted: false,
  };

  componentDidMount() {
    this._initApp();
  }

  _initApp() {
    initializeZoomBridge();
    this._checkAppVersion();
    this._checkAppMigration();
    this._initAuth();
  }

  _checkAppVersion() {
    getVersionApi()
    .then((data) => {
      this.setState({
        minAppVersion: data.APP_VERSION,
        forceUpdate: data.APP_FORCE_UPDATE,
      });
    })
    .catch((e) => {
      if (CONFIG.LOG) {
        console.log('App Version Check failed', e);
      }
    });
  }

  _checkAppMigration() {
    getMigrationStatusApi()
    .then((data) => {
      this.setState({
        migrateApp: data.APP_MIGRATE,
        androidLink: data.MIGRATE_ANDROID_LINK,
        iosLink: data.MIGRATE_IOS_LINK,
      });
    })
    .catch((e) => {
      if (CONFIG.LOG) {
        console.log('App Migration Check Failed', e);
      }
    });
  }

  _initAuth() {
    AsyncStorage.multiGet([
      TOKEN_STORAGE_KEY,
      ORGANIZATION_CODE_STORAGE_KEY,
    ])
    .then((results) => {
      if (results[1][1]) {
        this.props.setOrganizationCodeAction(results[1][1]);
      }
      if (results[0][1]) {
        this.props.setTokenAction(results[0][1]);
        this.setState({ tokenExisted: true });
      }
      return refreshTokenApi();
    })
    .then(({ token, ...user }) => {
      this.props.setTokenAction(token);
      this.props.setUserDataAction(user);
      this.props.loginAction();
    })
    .catch((e) => {
      if (CONFIG.LOG) {
        console.log('Auth Init Failed', e);
      }
      if (e.code && (e.code === 400 || e.code === 401)) {
        this.props.logoutAction({
          // eslint-disable-next-line
          tokenError: this.state.tokenExisted ? true : false,
        });
      } else {
        this.setState({
          retryScreenVisible: true,
        });
      }
    })
    .finally(() => {
      this._handleFinishLoading();
    });
  }

  _handleFinishLoading = () => {
    this.setState({ splashScreenVisible: false });
  };

  _renderAuthOrMainNavigator() {
    if (!this.props.loggedIn) {
      return (
        <AuthNavigator />
      );
    }
    return (
      <MainNavigator />
    );
  }

  render() {
    if (this.state.migrateApp) {
      return (
        <AppMigrate androidLink={this.state.androidLink} iosLink={this.state.iosLink} />
      );
    }

    if (this.state.forceUpdate && checkVersionObsolete(this.state.minAppVersion)) {
      return (
        <ForceUpdate />
      );
    }

    if (this.state.tokenExisted && this.state.retryScreenVisible) {
      return (
        <AppInitRetry
          onRetry={() => {
            this.setState({
              splashScreenVisible: true,
              retryScreenVisible: false,
            });
            this._initApp();
          }}
          onSignInAgain={() => {
            this.setState({
              retryScreenVisible: false,
            });
            this.props.logoutAction();
          }}
        />
      );
    }

    if (this.state.splashScreenVisible) {
      return (
        <SplashScreen />
      );
    }
    return (
      <NavigationContainer>
        <GalioProvider theme={MATERIAL_THEME}>
          <Block flex>
            {Platform.OS === 'ios' ? <StatusBar barStyle='default' /> : <StatusBar backgroundColor="#000" />}
            {this._renderAuthOrMainNavigator()}
          </Block>
        </GalioProvider>
      </NavigationContainer>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    loggedIn
  } = state.Auth;
  return {
    loggedIn
  };
};

export default connect(mapStateToProps, {
  loginAction,
  logoutAction,
  setUserDataAction,
  setTokenAction,
  setOrganizationCodeAction,
})(AppRoot);
