import React, { Fragment } from 'react';
import { View, ImageBackground, StyleSheet, Dimensions, ScrollView, RefreshControl, ActivityIndicator, FlatList, ToastAndroid } from 'react-native';
import base64 from 'base-64';
import { Block, Text, Icon, theme } from 'galio-framework';
import { connect } from 'react-redux';
import { getDashApi } from 'api/DashApi';
import { ONBOARDING } from 'assets/images';
import AssignmentCard from 'components/screenComponents/AssignmentCard';
import StreamCard from 'components/screenComponents/StreamCard';
import ConferenceCard from 'components/screenComponents/ConferenceCard';
import { ToastBox } from 'components';
import { MATERIAL_THEME, CONFIG, VALUES } from 'constants';
import { launchConference, getZoomEventEmitter } from 'utilities/ConferenceHelpers';

const { height, width } = Dimensions.get('screen');

class Onboarding extends React.Component {
  state = {
    loading: true,
    refreshSpinner: false,
    ongoing_streams: [],
    upcoming_streams: [],
    recent_assignments: [],
    ongoing_conferences: [],
    upcoming_conferences: [],
    error: false,
    conferenceRunning: false,
    errorBox: false,
    errorMessage: '',
  }

  componentDidMount() {
    this._fetchData();
    if (this.props.userData.reset_password) {
      this.props.navigation.navigate('ChangeDefaultPassword');
    }
    this.zoomEventEmitter = getZoomEventEmitter();
  }

  componentWillUnmount() {
    if (CONFIG.LOG) {
      console.log('Unmounting Home component');
    }
    if (this.meetingStatusChangedSubscription) {
      this.meetingStatusChangedSubscription.remove();
      this.meetingStatusChangedSubscription = undefined;
    }
  }

  _onMeetingStatusChange({ eventProperty }) {
    const meetingStatus = eventProperty.split(', ')[1].split('=')[1];
    if (CONFIG.LOG) {
      console.log('Meeting status listner triggered with status: ', meetingStatus);
    }
    if (
      meetingStatus === VALUES.CONFERENCE_STATUSES.ZOOM.MEETING_STATUS_DISCONNECTING
      || meetingStatus === VALUES.CONFERENCE_STATUSES.ZOOM.MEETING_STATUS_FAILED
    ) {
      if (CONFIG.LOG) {
        console.log('Ending Meeting in event listner');
      }
      this._onMeetingEnd();
    }
  }

  _onMeetingEnd() {
    if (CONFIG.LOG) {
      console.log('Meeting Ended');
    }
    this.setState({
      conferenceRunning: false
    });
    if (this.meetingStatusChangedSubscription) {
      this.meetingStatusChangedSubscription.remove();
      this.meetingStatusChangedSubscription = undefined;
    }
  }

  _onPress(conf) {
    if (this.state.conferenceRunning) {
      ToastAndroid.show('Meeting already running, please wait', ToastAndroid.SHORT);
      return;
    }
    if (conf.status !== 2) {
      this.setState({ errorBox: true, errorMessage: "This Live Class hasn't started yet. Try again later." });
      return;
    }
    const user = { ...this.props.userData };
    const conference = { ...conf };
    conference.provider_data = JSON.parse(base64.decode(conference.provider_data));
    this.setState({
      conferenceRunning: true,
    });
    if (this.meetingStatusChangedSubscription) {
      this.meetingStatusChangedSubscription.remove();
      this.meetingStatusChangedSubscription = undefined;
    }
    this.meetingStatusChangedSubscription = this.zoomEventEmitter.addListener(
      VALUES.CONFERENCE_EVENTS.ZOOM.MEETING_STATUS_CHANGED,
      (params) => {
        this._onMeetingStatusChange(params);
      }
    );
    launchConference({ conference, user })
    .catch((e) => {
      if (CONFIG.LOG) {
        console.log('There was some error in Zoom Conference');
        console.log(e);
      }
      this._onMeetingEnd();
    });
  }

  _fetchData(refreshSpinner) {
    if (refreshSpinner) {
      this.setState({ refreshSpinner: true });
    } else {
      this.setState({ loading: true });
    }
    getDashApi()
    .then(res => {
      this.setState({
        ongoing_streams: res.ongoing_streams,
        upcoming_streams: res.upcoming_streams,
        recent_assignments: res.recent_assignments,
        upcoming_conferences: res.upcoming_conferences,
        ongoing_conferences: res.ongoing_conferences,
        loading: false,
        refreshSpinner: false,
        error: false,
      });
    })
    .catch(() => {
      this.setState({ loading: false, refreshSpinner: false, error: true });
    });
  }

  _renderWelcomeText() {
    return (
      <ImageBackground
        source={ONBOARDING}
        style={{ flex: 1, height: this.state.loading ? 'auto' : height * 0.20, width, zIndex: 1 }}
      >
        <View style={styles.padded(this.state.loading)}>
          {
            this.state.loading &&
            <View style={{ flex: 1, justifyContent: 'center' }}>
              <View style={{ height: '20%' }} />
              <ActivityIndicator size={64} />
            </View>
          }
          <Block>
            <Text color="white" size={24}>Welcome,</Text>
          </Block>
          <Block row>
            <Text color="white" size={20}>to {this.props.userData.organization_name}</Text>
          </Block>
          {
            this.state.loading &&
            <Text size={16} color='rgba(255,255,255,0.9)' style={{ marginTop: 24 }}>
              A Distance Learning Solution
            </Text>
          }
        </View>
      </ImageBackground>
    );
  }

  renderAssignments = () => {
    const { recent_assignments } = this.state;
    if (recent_assignments.length > 0) {
      return (
        <Fragment>
          <View style={{ marginTop: 20 }}>
            <Text style={[MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE, { paddingHorizontal: theme.SIZES.BASE }]}>
              Recent Assignments
            </Text>
          </View>
          <Block flex>
            <FlatList
              data={recent_assignments}
              keyExtractor={item => item.id.toString()}
              renderItem={({ item }) => (<AssignmentCard assignment={item} navigation={this.props.navigation} />)}
            />
          </Block>
        </Fragment>
      );
    }
  }

  renderOngoingStreams = () => {
    const { ongoing_streams } = this.state;
    if (ongoing_streams.length > 0) {
      return (
        <Fragment>
          <View style={{ marginTop: 20 }}>
            <Text style={[MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE, { paddingHorizontal: theme.SIZES.BASE }]}>
              Ongoing Streams
            </Text>
          </View>
          <View style={{ paddingHorizontal: theme.SIZES.BASE }}>
            <Block flex>
              <FlatList
                data={ongoing_streams}
                keyExtractor={item => item.id.toString()}
                ListEmptyComponent={() => (<View />)}
                renderItem={({ item }) => (<StreamCard stream={item} onError={() => this.setState({ errorBox: true, errorMessage: 'This Stream could not be played' })} />)}
              />
            </Block>
          </View>
        </Fragment>
      );
    }
  }

  renderUpcomingStreams = () => {
    const { upcoming_streams } = this.state;
    if (upcoming_streams.length > 0) {
      return (
        <Fragment>
          <View style={{ marginTop: 20 }}>
            <Text style={[MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE, { paddingHorizontal: theme.SIZES.BASE }]}>
              Upcoming Streams
            </Text>
          </View>
          <View style={{ paddingHorizontal: theme.SIZES.BASE }}>
            <Block flex>
              <FlatList
                data={upcoming_streams}
                keyExtractor={item => item.id.toString()}
                ListEmptyComponent={() => (<View />)}
                renderItem={({ item }) => (<StreamCard disabled stream={item} navigation={this.props.navigation} />)}
              />
            </Block>
          </View>
        </Fragment>
      );
    }
  }

  renderUpcomingConferences = () => {
    const { upcoming_conferences } = this.state;
    if (upcoming_conferences.length > 0) {
      return (
        <Fragment>
          <View style={{ marginTop: 20 }}>
            <Text style={[MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE, { paddingHorizontal: theme.SIZES.BASE }]}>
              Upcoming Live Classes
            </Text>
          </View>
          <Block flex>
            <FlatList
              data={upcoming_conferences}
              keyExtractor={item => item.id.toString()}
              renderItem={({ item }) => (<ConferenceCard wide conference={item} onPress={({ conference }) => { this._onPress(conference); }} />)}
            />
          </Block>
        </Fragment>
      );
    }
  }

  renderOngoingConferences = () => {
    const { ongoing_conferences } = this.state;
    if (ongoing_conferences.length > 0) {
      return (
        <Fragment>
          <View style={{ marginTop: 20 }}>
            <Text style={[MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE, { paddingHorizontal: theme.SIZES.BASE }]}>
              Ongoing Live Classes
            </Text>
          </View>
          <Block flex>
            <FlatList
              data={ongoing_conferences}
              keyExtractor={item => item.id.toString()}
              renderItem={({ item }) => (<ConferenceCard wide conference={item} onPress={({ conference }) => { this._onPress(conference); }} />)}
            />
          </Block>
        </Fragment>
      );
    }
  }

  render() {
    const { ongoing_streams, upcoming_streams, recent_assignments, ongoing_conferences, upcoming_conferences } = this.state;
    const itemCount = ongoing_streams.length + upcoming_streams.length + recent_assignments.length + ongoing_conferences.length + upcoming_conferences.length;
    const fullHeight = itemCount === 0;
    return (
      <View style={{ flex: 1 }}>
        <Block flex style={styles.container}>
          <View style={{ flex: 1, height: 'auto', backgroundColor: MATERIAL_THEME.COLORS.DEFAULT }}>
            <ScrollView
              showsVerticalScrollIndicator={false}
              contentContainerStyle={fullHeight && { flex: 1, justifyContent: 'flex-start' }}
              refreshControl={
                <RefreshControl refreshing={this.state.refreshSpinner} onRefresh={() => this._fetchData(true)} />
              }
            >
              {this._renderWelcomeText()}
              {
                !fullHeight &&
                <View style={{ flex: 1, paddingTop: 20, paddingBottom: 20, backgroundColor: MATERIAL_THEME.COLORS.DEFAULT }}>
                  {this.renderOngoingConferences()}
                  {this.renderOngoingStreams()}
                  {this.renderAssignments()}
                  {this.renderUpcomingConferences()}
                  {this.renderUpcomingStreams()}
                </View>
              }
              {
                !this.state.loading && fullHeight &&
                <View style={{ flex: 1, backgroundColor: MATERIAL_THEME.COLORS.DEFAULT }}>
                  <View style={{ alignItems: 'center' }}>
                    <Icon
                      size={96}
                      name={this.state.error ? 'error-outline' : 'file'}
                      family={this.state.error ? 'material-icons' : 'feather'}
                      color={this.state.error ? 'red' : 'gray'}
                    />
                    <Text h5 muted>{this.state.error ? 'Some Error occured' : 'There is nothing coming up for you'}</Text>
                    <Text h6 muted>(pull down to refresh)</Text>
                  </View>
                </View>
              }
            </ScrollView>
          </View>
        </Block>
        <ToastBox
          isShow={this.state.errorBox}
          color="warning"
          message={this.state.errorMessage}
          position="bottom"
          offset={140}
          duration={3000}
          onHide={() => this.setState({ errorBox: false })}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.User.user,
  };
};

export default connect(mapStateToProps)(Onboarding);

const styles = StyleSheet.create({
  container: {
    backgroundColor: theme.COLORS.WHITE,
  },
  padded: (loading) => {
    return {
      flex: loading ? 1 : 0,
      paddingBottom: loading ? 120 : 32,
      height: loading ? 'auto' : height * 0.20,
      justifyContent: 'flex-end',
      zIndex: 2,
      paddingHorizontal: theme.SIZES.BASE * 2,
      backgroundColor: 'rgba(0,0,0,0.7)',
    };
  },
  button: {
    width: width - (theme.SIZES.BASE * 4),
    height: theme.SIZES.BASE * 3,
    shadowRadius: 0,
    shadowOpacity: 0,
  },
  gradient: {
    zIndex: 3,
    left: 0,
    right: 0,
    bottom: 0,
    height: 80,
    position: 'absolute',
  },
});
