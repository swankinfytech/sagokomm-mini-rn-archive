import React from 'react';
import { StyleSheet, Dimensions, FlatList, Text, ScrollView, RefreshControl, View } from 'react-native';
import { Block, theme } from 'galio-framework';
import { getStreamsApi } from 'api/StreamsApi';
import StreamCard from 'components/screenComponents/StreamCard';
import { SearchBar, Loader, ErrorBox, EmptyList, ToastBox } from 'components';
import { MATERIAL_THEME } from 'constants';

const { width } = Dimensions.get('screen');

export default class Streams extends React.Component {
  state = {
    loading: false,
    error: false,
    streams: [],
    search: '',
    errorBox: false,
  }

  componentDidMount() {
    this.refresh();
  }

  refresh() {
    this.setState({ loading: true });
    getStreamsApi()
    .then((res) => {
      this.setState({ loading: false, error: false, streams: res });
    })
    .catch(() => {
      this.setState({ loading: false, error: true });
    });
  }

  renderOngoingStreams = (streams) => {
    const filteredStreams = streams.filter(stream => {
      let display = true;
      let inGroup = false;
      stream.groups.forEach((group) => {
        inGroup = inGroup || group.name.toLowerCase().includes(this.state.search.toLowerCase());
      });
      display = stream.title.toLowerCase().includes(this.state.search.toLowerCase()) ||
        stream.description.toLowerCase().includes(this.state.search.toLowerCase()) ||
        inGroup;
      return display;
    });

    return (
      <Block flex>
        <FlatList
          data={filteredStreams}
          keyExtractor={item => item.id.toString()}
          ListEmptyComponent={() => (<View />)}
          renderItem={({ item }) => (<StreamCard stream={item} onError={() => this.setState({ errorBox: true })} />)}
        />
      </Block>
    );
  }

  renderUpcomingStreams = (streams) => {
    const filteredStreams = streams.filter(stream => {
      let display = true;
      let inGroup = false;
      stream.groups.forEach((group) => {
        inGroup = inGroup || group.name.toLowerCase().includes(this.state.search.toLowerCase());
      });
      display = stream.title.toLowerCase().includes(this.state.search.toLowerCase()) ||
        stream.description.toLowerCase().includes(this.state.search.toLowerCase()) ||
        inGroup;
      return display;
    });

    return (
      <Block flex>
        <FlatList
          data={filteredStreams}
          keyExtractor={item => item.id.toString()}
          ListEmptyComponent={() => (<View />)}
          renderItem={({ item }) => (<StreamCard disabled stream={item} navigation={this.props.navigation} />)}
        />
      </Block>
    );
  }

  render() {
    if (this.state.loading) {
      return (<Loader />);
    }

    if (this.state.error) {
      return (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.emptyErrorContainer}
          refreshControl={<RefreshControl refreshing={false} onRefresh={() => this.refresh()} />}
        >
          <ErrorBox />
        </ScrollView>
      );
    }

    const ongoingStreams = this.state.streams.filter(stream => {
      return stream.status === 2;
    });

    const upcomingStreams = this.state.streams.filter(stream => {
      return stream.status === 1;
    });

    if (upcomingStreams.length === 0 && ongoingStreams.length === 0) {
      return (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.emptyErrorContainer}
          refreshControl={<RefreshControl refreshing={false} onRefresh={() => this.refresh()} />}
        >
          <EmptyList />
        </ScrollView>
      );
    }

    return (
      <View style={{ flex: 1 }}>
        <Block flex center style={styles.home}>
          <SearchBar
            value={this.state.search}
            onChangeText={(val) => this.setState({ search: val })}
            placeholder="search by Title, Description or Group"
          />
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.listContainer}
            refreshControl={
              <RefreshControl refreshing={false} onRefresh={() => this.refresh()} />
            }
          >
            <Block style={styles.sectionContainer}>
              {
                ongoingStreams.length > 0 &&
                <Text style={MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE}>
                  Ongoing Streams
                </Text>
              }
              {this.renderOngoingStreams(ongoingStreams)}
              <View style={{ margin: ongoingStreams.length > 0 ? 10 : 0 }} />
              {
                upcomingStreams.length > 0 &&
                <Text style={MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE}>
                  Upcoming Streams
                </Text>
              }
              {this.renderUpcomingStreams(upcomingStreams)}
            </Block>
          </ScrollView>
        </Block>
        <ToastBox
          isShow={this.state.errorBox}
          color="warning"
          message="Stream could not be played"
          position="bottom"
          offset={140}
          duration={3000}
          onHide={() => this.setState({ errorBox: false })}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width,
  },
  emptyErrorContainer: {
    flex: 1,
  },
  listContainer: {
    width: width - (theme.SIZES.BASE * 2),
  },
  sectionContainer: {
    paddingBottom: theme.SIZES.BASE,
    paddingTop: theme.SIZES.BASE,
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
    width: width - (theme.SIZES.BASE * 2),
    paddingVertical: theme.SIZES.BASE * 2,
  },
});
