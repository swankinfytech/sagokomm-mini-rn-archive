import React from 'react';
import { Dimensions, Text, View, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { Block, theme, Input, Button, Icon } from 'galio-framework';
import { connect } from 'react-redux';
import { Loader, Select, FeedbackModal, ToastBox } from 'components';
import { MATERIAL_THEME } from 'constants';
import { sendFeedbackApi } from 'api/FeedbackApi';

const { width } = Dimensions.get('screen');
const stars = [1, 2, 3, 4, 5];

class Feedback extends React.Component {
  state = {
    loading: false,
    invalid: {},
    error: false,
    success: false,
    title: '',
    type: 'Feedback',
    message: '',
    rating: 0,
    feedbackModal: false,
    toastShow: false,
    toastColor: 'warning',
    toastMessage: 'Feedback submission failed',
  }

  setRating(rating) {
    this.setState({ rating });
  }

  updateValue(e, key) {
    const state = { ...this.state };
    state[key] = e.nativeEvent.text;
    this.setState({ ...state });
  }

  submit() {
    this.setState({ success: false });
    if (this.validate()) {
      this.setState({ loading: true, error: false });
      sendFeedbackApi({
        name: this.props.userData.name,
        username: this.props.userData.email,
        mobile: this.props.userData.mobile,
        domain: this.props.userData.domain,
        organization_name: this.props.userData.organization_name,
        title: this.state.title,
        type: this.state.type,
        message: `${this.state.rating} stars -\n${this.state.message}`,
      })
      .then(() => {
        this.setState({
          loading: false,
          success: true,
          title: '',
          type: 'Feedback',
          message: '',
          feedbackModal: this.state.rating > 2,
          toastShow: true,
          toastColor: 'success',
          toastMessage: 'Feedback submitted successfully',
        });
      })
      .catch((e) => {
        console.log(e.response);
        this.setState({
          loading: false,
          error: true,
          toastShow: true,
          toastColor: 'warning',
          toastMessage: 'Feedback submission failed',
        });
      });
    } else {
      this.setState({ toastShow: true, toastColor: 'warning', toastMessage: 'Incomplete fields found' });
    }
  }

  validate() {
    const { title, rating } = this.state;
    const { invalid } = this.state;
    invalid.title = title === '';
    invalid.rating = rating === 0;
    this.setState({ invalid });
    return title !== '' && rating > 0;
  }

  renderInputs = (key, title, placeholder, multiline, required) => {
    return (
      <View style={styles.group}>
        <View style={{ flexDirection: 'row' }}>
          <Text bold size={16} style={styles.title}>{title}</Text>
          {
            required &&
            <Text bold size={16} style={styles.asterisk}>*</Text>
          }
        </View>
        <Block style={{ paddingHorizontal: theme.SIZES.BASE }}>
          <Input
            multiline={multiline}
            placeholder={placeholder}
            value={this.state[key]}
            onChange={(e) => this.updateValue(e, key)}
            placeholderTextColor={MATERIAL_THEME.COLORS.DEFAULT}
            color="#000"
            style={styles.input(multiline, this.state.invalid[key])}
          />
        </Block>
      </View>
    );
  }

  renderStars() {
    return (
      <View style={{ alignSelf: 'center', marginTop: theme.SIZES.BASE * 2 }}>
        <Text style={{ color: MATERIAL_THEME.COLORS.MUTED, textAlign: 'center', fontSize: 16 }}>Rate our services</Text>
        <View style={{ flexDirection: 'row' }}>
          {
            stars.map((star, index) => (
              <TouchableWithoutFeedback key={index} onPress={() => this.setRating(star)}>
                <Icon
                 style={{ margin: theme.SIZES.BASE / 4 }}
                  size={40}
                  name="star"
                  family="font-awesome"
                  color={star <= this.state.rating ? MATERIAL_THEME.COLORS.WARNING : MATERIAL_THEME.COLORS.MUTED}
                />
              </TouchableWithoutFeedback>
            ))
          }
        </View>
        {
          this.state.invalid.rating &&
          <Text style={{ color: MATERIAL_THEME.COLORS.ERROR, textAlign: 'center', fontSize: 12 }}>Please provide a rating</Text>
        }
      </View>
    );
  }

  render() {
    return (
      <View style={styles.home}>
        {
          this.state.loading ?
          <Loader /> :
          <ScrollView>
            {this.renderStars()}
            <View style={{ paddingLeft: theme.SIZES.BASE, paddingTop: theme.SIZES.BASE * 2 }}>
              <Select
                options={['Feedback', 'Bug Report', 'Enquiry']}
                style={styles.shadow}
                value={this.state.type}
                onSelect={(index, value) => this.setState({ type: value })}
              />
            </View>
            {this.renderInputs('title', 'Title', 'Enter title', false, true)}
            {this.renderInputs('message', 'Message', 'Enter message', true)}
            <View style={styles.buttonContainer}>
              <Button color="info" style={{ width: 200 }} onPress={() => this.submit()}>Submit</Button>
            </View>
          </ScrollView>
        }
        <FeedbackModal
          visible={this.state.feedbackModal}
          dismissModal={() => this.setState({ feedbackModal: false })}
          error={() => this.setState({ toastShow: true, toastColor: 'warning', toastMessage: 'App Store could not be reached' })}
        />
        <ToastBox
          isShow={this.state.toastShow}
          color={this.state.toastColor}
          message={this.state.toastMessage}
          position="bottom"
          offset={140}
          duration={3000}
          onHide={() => this.setState({ toastShow: false })}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userData: state.User.user,
  };
};

export default connect(mapStateToProps)(Feedback);

const styles = {
  home: {
    width,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  group: {
    paddingTop: theme.SIZES.BASE * 2,
  },
  title: {
    paddingVertical: theme.SIZES.BASE / 4,
    paddingLeft: theme.SIZES.BASE * 1.2,
  },
  asterisk: {
    color: MATERIAL_THEME.COLORS.ERROR,
    marginLeft: 4,
    marginTop: 2,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    paddingTop: theme.SIZES.BASE * 3,
    paddingBottom: theme.SIZES.BASE * 3,
    paddingHorizontal: theme.SIZES.BASE,
  },
  input: (multiline, error) => {
    return {
      width: width - (theme.SIZES.BASE * 2),
      borderTopWidth: error ? null : 0,
      borderLeftWidth: error ? null : 0,
      borderRightWidth: error ? null : 0,
      borderColor: error ? MATERIAL_THEME.COLORS.ERROR : MATERIAL_THEME.COLORS.INPUT,
      height: multiline ? 100 : null,
    };
  },
  shadow: {
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 0.2,
    elevation: 2,
  },
  errorBox: {
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: MATERIAL_THEME.COLORS.ERROR,
    paddingVertical: theme.SIZES.BASE,
  },
  successBox: {
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 10,
    borderColor: MATERIAL_THEME.COLORS.SUCCESS,
    paddingVertical: theme.SIZES.BASE,
  },
};
