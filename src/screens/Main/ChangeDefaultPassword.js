import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import {
  Block,
  theme,
  Input,
  Button,
} from 'galio-framework';
import { LOGO, SLANT_STRIP } from 'assets/images';
import { LANG, MATERIAL_THEME, VALUES } from 'constants';
import {
  passwordValidator,
  equalityValidator
} from 'validators';
import { changeDefaultPasswordApi } from 'api/AuthApi';

const { height, width } = Dimensions.get('window');

export default class extends Component {
  state = {
    password: '',
    passwordConfirmation: '',
    loading: false,
    formInvalid: false,
    requestFailed: false,
    resetComplete: false,
  }

  _isFormValid() {
    if (
      !passwordValidator(this.state.password)
      || !passwordValidator(this.state.passwordConfirmation)
      || !equalityValidator(this.state.password, this.state.passwordConfirmation)
    ) {
      return false;
    }
    return true;
  }

  _onButtonPress() {
    this.setState({
      loading: true,
      formInvalid: false,
      requestFailed: false,
    });
    if (!this._isFormValid()) {
      this.setState({
        loading: false,
        formInvalid: true,
      });
      return;
    }
    changeDefaultPasswordApi({
      password: this.state.password,
      password_confirmation: this.state.passwordConfirmation,
    })
    .then(() => {
      this.setState({
        loading: false,
        resetComplete: true,
      });
    })
    .catch(() => {
      this.setState({
        loading: false,
        requestFailed: true,
      });
    });
  }

  _renderErrorMessages() {
    if (this.state.formInvalid) {
      return (
        <Text style={styles.errorMessageStyle}>
          Please Enter A Valid Password
        </Text>
      );
    }
    if (this.state.requestFailed) {
      return (
        <Text style={styles.errorMessageStyle}>
          Password Reset Failed
        </Text>
      );
    }
  }

  _renderLoaderOrText() {
    if (this.state.loading) {
      return (
        <ActivityIndicator
          color={theme.COLORS.WHITE}
        />
      );
    }
    return (
      <Text style={styles.buttonTextStyle}>
        CHANGE PASSWORD
      </Text>
    );
  }

  _renderBack() {
    return (
      <View style={styles.backContainerStyle}>
        <View style={{ marginTop: -4, alignItems: 'center' }}>
          <Text style={{ color: theme.COLORS.ICON, fontSize: 18 }}>Change Password</Text>
        </View>
      </View>
    );
  }

  _renderCardTop() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        <View style={styles.logoContainerStyle}>
          <Image source={LOGO} style={styles.logoStyle} />
        </View>
        {
          this.state.resetComplete ?
          <View>
            <Text style={styles.infoMessageStyle}>
              Your password has been changed. You can now login with your new password.
            </Text>
          </View> :
          <View>
            <Text style={styles.infoMessageStyle}>
              Change your Default Password to secure your Account
            </Text>
            <Input
              style={styles.transparentInputStyle}
              placeholder={LANG.PLACEHOLDERS.passwordReset}
              color={theme.COLORS.ICON}
              returnKeyType='done'
              placeholderTextColor={MATERIAL_THEME.COLORS.MUTED}
              value={this.state.password}
              onChangeText={(password) => { this.setState({ password }); }}
              password
              viewPass
              iconColor={theme.COLORS.MUTED}
            />
            <Input
              style={styles.transparentInputStyle}
              placeholder={LANG.PLACEHOLDERS.passwordConfirmation}
              color={theme.COLORS.ICON}
              returnKeyType='done'
              placeholderTextColor={MATERIAL_THEME.COLORS.MUTED}
              value={this.state.passwordConfirmation}
              onChangeText={(passwordConfirmation) => { this.setState({ passwordConfirmation }); }}
              password
              viewPass
              iconColor={theme.COLORS.MUTED}
            />
          </View>
        }
      </View>
    );
  }

  _renderCardBottom() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, marginTop: -(theme.SIZES.BASE * 2), marginBottom: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        {
          this.state.resetComplete ?
          <View>
            <Button
              style={styles.buttonStyle}
              color={MATERIAL_THEME.COLORS.SUCCESS}
              onPress={() => { this.props.navigation.goBack(); }}
              disabled={this.state.loading}
            >
              <Text style={styles.buttonTextStyle}>
                CONTINUE
              </Text>
            </Button>
          </View> :
          <View>
            <Button
              style={styles.buttonStyle}
              color={MATERIAL_THEME.COLORS.SUCCESS}
              onPress={() => { this._onButtonPress(); }}
              disabled={this.state.loading}
            >
              {this._renderLoaderOrText()}
            </Button>
          </View>
        }
        {this._renderErrorMessages()}
      </View>
    );
  }

  _renderCardContents() {
    return (
      <View style={styles.cardContentContaier}>
        {this._renderCardTop()}
        <Image source={SLANT_STRIP} style={styles.slantStyle} />
        {this._renderCardBottom()}
      </View>
    );
  }

  _renderFooter() {
    return (
      <Block style={styles.bottomTextContainer} center>
        <Text style={styles.bottomText}>Powered By Swank Infytech Pvt Ltd</Text>
      </Block>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND }}>
        <ScrollView contentContainerStyle={styles.containerStyle}>
          {this._renderBack()}
          <View style={styles.card(this.state.resetComplete)}>
            {this._renderCardContents()}
          </View>
        </ScrollView>
        {this._renderFooter()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND,
  },
  backContainerStyle: {
    paddingTop: VALUES.iPhoneX ? theme.SIZES.BASE * 5 : theme.SIZES.BASE * 2,
    marginBottom: theme.SIZES.BASE / 2,
  },
  slantStyle: {
    width,
    height: width * 0.32,
    marginLeft: -(width / 10),
  },
  card: (short) => {
    const diff = short ? 580 : 650;
    return {
      margin: width / 10,
      marginTop: (height - diff) / 2,
      backgroundColor: '#fff',
      borderRadius: 16,
      shadowColor: '#000000',
      shadowOffset: { width: 0, height: 4 },
      shadowRadius: 1,
      shadowOpacity: 0.6,
      elevation: 4,
    };
  },
  cardContentContaier: {
    flex: 0,
    width: '80%',
  },
  bottomTextContainer: {
    marginBottom: 12,
  },
  bottomText: {
    color: theme.COLORS.WHITE
  },
  logoContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: theme.SIZES.BASE * 2,
  },
  logoStyle: {
    width: 100,
    height: 100,
  },
  transparentInputStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: MATERIAL_THEME.COLORS.LIGHT_GREY,
  },
  buttonStyle: {
    marginTop: theme.SIZES.BASE * 2,
    width: '80%',
    alignSelf: 'center',
  },
  buttonTextStyle: {
    color: theme.COLORS.WHITE,
    fontSize: theme.SIZES.FONT,
  },
  errorMessageStyle: {
    alignSelf: 'center',
    marginTop: theme.SIZES.BASE,
    color: theme.COLORS.ERROR,
    fontWeight: 'bold',
  },
  infoMessageStyle: {
    color: theme.COLORS.ICON,
    marginBottom: theme.SIZES.BASE,
    fontSize: 16,
    textAlign: 'center',
  },
});
