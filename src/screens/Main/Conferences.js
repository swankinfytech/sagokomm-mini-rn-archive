import React from 'react';
import { connect } from 'react-redux';
import { StyleSheet, Dimensions, FlatList, Text, ScrollView, RefreshControl, View, ToastAndroid } from 'react-native';
import base64 from 'base-64';
import { Block, theme } from 'galio-framework';
import { getConferencesApi } from 'api/ConferencesApi';
import ConferenceCard from 'components/screenComponents/ConferenceCard';
import { SearchBar, Loader, ErrorBox, EmptyList, ToastBox } from 'components';
import { MATERIAL_THEME, CONFIG, VALUES } from 'constants';
import { launchConference, getZoomEventEmitter } from 'utilities/ConferenceHelpers';

const { width } = Dimensions.get('screen');

class Conferences extends React.Component {
  state = {
    loading: false,
    error: false,
    conferences: [],
    search: '',
    conferenceRunning: false,
    errorBox: false,
    errorMessage: '',
  }

  componentDidMount() {
    this._fetchData();
    this.zoomEventEmitter = getZoomEventEmitter();
  }

  componentWillUnmount() {
    if (CONFIG.LOG) {
      console.log('Unmounting conference component');
    }
    if (this.meetingStatusChangedSubscription) {
      this.meetingStatusChangedSubscription.remove();
      this.meetingStatusChangedSubscription = undefined;
    }
  }

  _onMeetingStatusChange({ eventProperty }) {
    const meetingStatus = eventProperty.split(', ')[1].split('=')[1];
    if (CONFIG.LOG) {
      console.log('Meeting status listner triggered with status: ', meetingStatus);
    }
    if (
      meetingStatus === VALUES.CONFERENCE_STATUSES.ZOOM.MEETING_STATUS_DISCONNECTING
      || meetingStatus === VALUES.CONFERENCE_STATUSES.ZOOM.MEETING_STATUS_FAILED
    ) {
      if (CONFIG.LOG) {
        console.log('Ending Meeting in event listner');
      }
      this._onMeetingEnd();
    }
  }

  _onMeetingEnd() {
    if (CONFIG.LOG) {
      console.log('Meeting Ended');
    }
    this.setState({
      conferenceRunning: false
    });
    if (this.meetingStatusChangedSubscription) {
      this.meetingStatusChangedSubscription.remove();
      this.meetingStatusChangedSubscription = undefined;
    }
  }

  _fetchData() {
    this.setState({ loading: true });
    getConferencesApi()
    .then((res) => {
      this.setState({ loading: false, error: false, conferences: res });
    })
    .catch(() => {
      this.setState({ loading: false, error: true });
    });
  }

  _onPress(conf) {
    if (this.state.conferenceRunning) {
      ToastAndroid.show('Meeting already running, please wait', ToastAndroid.SHORT);
      return;
    }
    if (conf.status !== 2) {
      this.setState({ errorBox: true, errorMessage: "This Live Class hasn't started yet. Try again later." });
      return;
    }
    const { user } = this.props;
    this.setState({
      conferenceRunning: true,
    });
    const conference = { ...conf };
    conference.provider_data = JSON.parse(base64.decode(conference.provider_data));
    if (this.meetingStatusChangedSubscription) {
      this.meetingStatusChangedSubscription.remove();
      this.meetingStatusChangedSubscription = undefined;
    }
    this.meetingStatusChangedSubscription = this.zoomEventEmitter.addListener(
      VALUES.CONFERENCE_EVENTS.ZOOM.MEETING_STATUS_CHANGED,
      (params) => {
        this._onMeetingStatusChange(params);
      }
    );
    launchConference({ conference, user })
    .catch((e) => {
      if (CONFIG.LOG) {
        console.log('There was some error in Zoom Conference');
        console.log(e);
      }
      this._onMeetingEnd();
    });
  }

  _renderOngoingConferences = (conferences) => {
    const filteredConferences = conferences.filter(conference => {
      let display = true;
      let inGroup = false;
      conference.groups.forEach((group) => {
        inGroup = inGroup || group.name.toLowerCase().includes(this.state.search.toLowerCase());
      });
      display = conference.title.toLowerCase().includes(this.state.search.toLowerCase()) ||
        inGroup;
      return display;
    });

    return (
      <Block flex>
        <FlatList
          data={filteredConferences}
          keyExtractor={item => item.id.toString()}
          ListEmptyComponent={() => (<View />)}
          renderItem={({ item }) => (<ConferenceCard conference={item} onPress={({ conference }) => { this._onPress(conference); }} />)}
        />
      </Block>
    );
  }

  _renderUpcomingConferences = (conferences) => {
    const filteredConferences = conferences.filter(conference => {
      let display = true;
      let inGroup = false;
      conference.groups.forEach((group) => {
        inGroup = inGroup || group.name.toLowerCase().includes(this.state.search.toLowerCase());
      });
      display = conference.title.toLowerCase().includes(this.state.search.toLowerCase()) ||
        inGroup;
      return display;
    });

    return (
      <Block flex>
        <FlatList
          data={filteredConferences}
          keyExtractor={item => item.id.toString()}
          ListEmptyComponent={() => (<View />)}
          renderItem={({ item }) => (<ConferenceCard conference={item} onPress={({ conference }) => { this._onPress(conference); }} />)}
        />
      </Block>
    );
  }

  render() {
    if (this.state.loading) {
      return (<Loader />);
    }

    if (this.state.error) {
      return (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.emptyErrorContainer}
          refreshControl={<RefreshControl refreshing={false} onRefresh={() => this._fetchData()} />}
        >
          <ErrorBox />
        </ScrollView>
      );
    }

    const ongoingConferences = this.state.conferences.filter(conference => {
      return conference.status === 2;
    });

    const upcomingConferences = this.state.conferences.filter(conference => {
      return conference.status === 1;
    });

    if (upcomingConferences.length === 0 && ongoingConferences.length === 0) {
      return (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.emptyErrorContainer}
          refreshControl={<RefreshControl refreshing={false} onRefresh={() => this._fetchData()} />}
        >
          <EmptyList />
        </ScrollView>
      );
    }

    return (
      <Block flex center style={styles.home}>
        <SearchBar
          value={this.state.search}
          onChangeText={(val) => this.setState({ search: val })}
          placeholder="search by Title, Description or Group"
        />
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.listContainer}
          refreshControl={
            <RefreshControl refreshing={false} onRefresh={() => this._fetchData()} />
          }
        >
          <Block style={styles.sectionContainer}>
            {
              ongoingConferences.length > 0 &&
              <Text style={MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE}>
                Ongoing Live Classes
              </Text>
            }
            {this._renderOngoingConferences(ongoingConferences)}
            <View style={{ margin: ongoingConferences.length > 0 ? 10 : 0 }} />
            {
              upcomingConferences.length > 0 &&
              <Text style={MATERIAL_THEME.REUSABLE_STYLES.SECTION_TITLE}>
                Upcoming Live Classes
              </Text>
            }
            {this._renderUpcomingConferences(upcomingConferences)}
          </Block>
        </ScrollView>
        <ToastBox
          isShow={this.state.errorBox}
          color="warning"
          message={this.state.errorMessage}
          position="bottom"
          offset={140}
          duration={3000}
          onHide={() => this.setState({ errorBox: false })}
        />
      </Block>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    user: state.User.user,
  };
};

export default connect(mapStateToProps)(Conferences);

const styles = StyleSheet.create({
  home: {
    width,
  },
  emptyErrorContainer: {
    flex: 1,
  },
  listContainer: {
    width: width - (theme.SIZES.BASE * 1.5),
  },
  sectionContainer: {
    paddingBottom: theme.SIZES.BASE,
    paddingTop: theme.SIZES.BASE,
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
    width: width - (theme.SIZES.BASE * 2),
    paddingVertical: theme.SIZES.BASE * 2,
  },
});
