import React from 'react';
import { StyleSheet, Dimensions, FlatList, ScrollView, RefreshControl } from 'react-native';
import { Block, theme } from 'galio-framework';
import { getAssignmentsApi } from 'api/AssignmentsApi';
import AssignmentCard from 'components/screenComponents/AssignmentCard';
import { SearchBar, Loader, ErrorBox, EmptyList } from 'components';

const { width } = Dimensions.get('screen');

export default class Assignments extends React.Component {
  state = {
    loading: false,
    error: false,
    assignments: [],
    search: '',
  }

  componentDidMount() {
    this.refresh();
  }

  refresh() {
    this.setState({ loading: true });
    getAssignmentsApi()
    .then((res) => {
      this.setState({ loading: false, error: false, assignments: res });
    })
    .catch(() => {
      this.setState({ loading: false, error: true });
    });
  }

  renderItems = () => {
    const assignments = this.state.assignments.filter(assignment => {
      let display = true;
      display = assignment.title.toLowerCase().includes(this.state.search.toLowerCase()) ||
        assignment.message.toLowerCase().includes(this.state.search.toLowerCase()) ||
        assignment.group_name.toLowerCase().includes(this.state.search.toLowerCase());
      return display;
    });

    return (
      <Block flex>
        <FlatList
          data={assignments}
          keyExtractor={item => item.id.toString()}
          onRefresh={() => this.refresh()}
          refreshing={this.state.loading}
          renderItem={({ item }) => (<AssignmentCard wide assignment={item} navigation={this.props.navigation} />)}
        />
      </Block>
    );
  }

  render() {
    if (this.state.loading) {
      return (<Loader />);
    }

    if (this.state.error) {
      return (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.emptyErrorContainer}
          refreshControl={<RefreshControl refreshing={false} onRefresh={() => this.refresh()} />}
        >
          <ErrorBox />
        </ScrollView>
      );
    }

    if (this.state.assignments.length === 0) {
      return (
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.emptyErrorContainer}
          refreshControl={<RefreshControl refreshing={false} onRefresh={() => this.refresh()} />}
        >
          <EmptyList />
        </ScrollView>
      );
    }

    return (
      <Block flex center style={styles.home}>
        <SearchBar
          value={this.state.search}
          onChangeText={(val) => this.setState({ search: val })}
          placeholder="search by Title, Message or Group"
        />
        {this.renderItems()}
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  home: {
    width,
  },
  emptyErrorContainer: {
    flex: 1,
  },
  header: {
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
    zIndex: 2,
  },
  tabs: {
    marginBottom: 24,
    marginTop: 10,
    elevation: 4,
  },
  tab: {
    backgroundColor: theme.COLORS.TRANSPARENT,
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  tabTitle: {
    lineHeight: 19,
    fontWeight: '300'
  },
  divider: {
    borderRightWidth: 0.3,
    borderRightColor: theme.COLORS.MUTED,
  },
  products: {
    width: width - (theme.SIZES.BASE * 2),
    paddingVertical: theme.SIZES.BASE * 2,
  },
});
