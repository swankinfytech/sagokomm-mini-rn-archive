import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  StyleSheet,
  ActivityIndicator,
  TextInput,
  Dimensions,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {
  Block,
  theme,
  Button,
} from 'galio-framework';
import { LOGO, SLANT_STRIP } from 'assets/images';
import { VALUES, MATERIAL_THEME, CONFIG } from 'constants';
import {
  integerValidator,
  requiredValidator,
} from 'validators';
import { verifyOtpApi } from 'api/OtpApi';

const { height, width } = Dimensions.get('window');

export default class extends Component {
  state = {
    otp: '',
    loading: false,
    formInvalid: false,
    requestFailed: false,
    errorMessage: '',
  }

  _isFormValid() {
    if (
      !integerValidator(this.state.otp)
      || !requiredValidator(this.state.otp)
    ) {
      return false;
    }
    return true;
  }

  _onButtonPress() {
    this.setState({
      loading: true,
      formInvalid: false,
      requestFailed: false,
    });
    if (!this._isFormValid()) {
      this.setState({
        loading: false,
        formInvalid: true,
      });
      return;
    }
    verifyOtpApi({
      mobile_number: this.props.route.params.mobile_number,
      otp: this.state.otp,
      call_func: 'generate_forgot_token',
      func_data: {
        email: this.props.route.params.email
      },
    })
    .then((data) => {
      this.setState({
        loading: false,
      });
      this.props.navigation.navigate('ResetPassword', {
        email: this.props.route.params.email,
        token: data.token,
      });
    })
    .catch(() => {
      this.setState({
        loading: false,
        requestFailed: true,
      });
    });
  }

  _renderErrorMessages() {
    if (this.state.formInvalid) {
      return (
        <Text style={styles.errorMessageStyle}>
          Please Enter Valid OTP
        </Text>
      );
    }
    if (this.state.requestFailed) {
      return (
        <Text style={styles.errorMessageStyle}>
          Invalid OTP
        </Text>
      );
    }
  }

  _renderLoaderOrText() {
    if (this.state.loading) {
      return (
        <ActivityIndicator
          color={theme.COLORS.WHITE}
        />
      );
    }
    return (
      <Text style={styles.buttonTextStyle}>
        VERIFY OTP
      </Text>
    );
  }

  _renderOTPMessageOrOTPError() {
    const message = `An OTP has been sent to +91-${this.props.route.params.mobile_number}`;
    return (
      <Text style={styles.infoMessageStyle}>
        {message}
      </Text>
    );
  }

  _renderBack() {
    return (
      <View style={styles.backContainerStyle}>
        <Button
          style={styles.backButtonStyle}
          color={theme.COLORS.ICON}
          onPress={() => { this.props.navigation.goBack(); }}
          disabled={this.state.loading}
        >
          <Text style={{ color: theme.COLORS.WHITE }}>Back</Text>
        </Button>
        <View style={{ marginTop: -4, alignItems: 'center' }}>
          <Text style={{ color: theme.COLORS.ICON, fontSize: 20 }}>Verify OTP</Text>
        </View>
      </View>
    );
  }

  _renderCardTop() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        <View style={styles.logoContainerStyle}>
          <Image source={LOGO} style={styles.logoStyle} />
        </View>
        {this._renderOTPMessageOrOTPError()}
        <TextInput
          style={styles.transparentInputStyle}
          placeholder="XXXX"
          placeholderTextColor={theme.COLORS.MUTED}
          maxLength={4}
          returnKeyType='done'
          value={this.state.otp}
          onChangeText={(otp) => { this.setState({ otp }); }}
          keyboardType='numeric'
        />
      </View>
    );
  }

  _renderCardBottom() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, marginTop: -(theme.SIZES.BASE * 2), marginBottom: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        <Button
          style={styles.buttonStyle}
          color={MATERIAL_THEME.COLORS.SUCCESS}
          onPress={() => { this._onButtonPress(); }}
          disabled={this.state.loading}
        >
          {this._renderLoaderOrText()}
        </Button>
        {this._renderErrorMessages()}
      </View>
    );
  }

  _renderCardContents() {
    return (
      <View style={styles.cardContentContaier}>
        {this._renderCardTop()}
        <Image source={SLANT_STRIP} style={styles.slantStyle} />
        {this._renderCardBottom()}
      </View>
    );
  }

  _renderFooter() {
    return (
      <TouchableOpacity onPress={() => { Linking.openURL('http://sagokomm.com').catch((err) => CONFIG.LOG && console.error('An error occurred', err)); }}>
        <Block style={styles.bottomTextContainer} center>
          <Text style={styles.bottomText}>Powered By Sagokomm</Text>
        </Block>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND }}>
        <ScrollView contentContainerStyle={styles.containerStyle}>
          {this._renderBack()}
          <View style={styles.card}>
            {this._renderCardContents()}
          </View>
        </ScrollView>
        {this._renderFooter()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND,
  },
  backContainerStyle: {
    paddingTop: VALUES.iPhoneX ? theme.SIZES.BASE * 5 : theme.SIZES.BASE * 2,
    marginBottom: theme.SIZES.BASE / 2,
  },
  slantStyle: {
    width,
    height: width * 0.32,
    marginLeft: -(width / 10),
  },
  card: {
    margin: width / 10,
    marginTop: (height - 590) / 2,
    backgroundColor: '#fff',
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 1,
    shadowOpacity: 0.6,
    elevation: 4,
  },
  cardContentContaier: {
    flex: 0,
    width: '80%',
  },
  bottomTextContainer: {
    marginBottom: 12,
  },
  bottomText: {
    color: theme.COLORS.WHITE,
  },
  logoContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: theme.SIZES.BASE * 2,
  },
  logoStyle: {
    width: 100,
    height: 100,
  },
  transparentInputStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    fontSize: 25,
    textAlign: 'center',
    alignSelf: 'center',
    paddingBottom: 15,
    color: theme.COLORS.ICON,
    width: '50%',
  },
  buttonStyle: {
    marginTop: theme.SIZES.BASE * 2,
    width: '60%',
    alignSelf: 'center',
  },
  buttonTextStyle: {
    color: theme.COLORS.WHITE,
    fontSize: theme.SIZES.FONT,
  },
  errorMessageStyle: {
    alignSelf: 'center',
    marginTop: theme.SIZES.BASE,
    color: theme.COLORS.ERROR,
    fontWeight: 'bold',
  },
  infoMessageStyle: {
    color: theme.COLORS.ICON,
    marginBottom: theme.SIZES.BASE,
    fontSize: 16,
    textAlign: 'center',
  },
  backButtonStyle: {
    position: 'absolute',
    top: theme.SIZES.BASE * 1.5,
    left: theme.SIZES.BASE * 1.5,
    width: 60,
    height: 30,
    borderRadius: 15,
  },
});
