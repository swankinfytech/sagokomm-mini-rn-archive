import React from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
  Text,
  Image,
  ActivityIndicator,
  Dimensions,
  StatusBar,
  Linking,
} from 'react-native';
import { connect } from 'react-redux';
import OneSignal from 'react-native-onesignal';
import { Block, theme, Input, Button } from 'galio-framework';
import { LOGO, SLANT_STRIP } from 'assets/images';
import { LANG, MATERIAL_THEME, VALUES, CONFIG } from 'constants';
import {
  setTokenAction,
  setOrganizationCodeAction,
  loginAction,
} from 'actions/AuthActions';
import { setUserDataAction } from 'actions/UserActions';
import { loginApi } from 'api/AuthApi';
import {
  requiredStringValidator,
  passwordValidator,
} from 'validators';
import { networkError } from 'utilities/AxiosHelpers';

const { width } = Dimensions.get('window');

class Login extends React.Component {
  state = {
    organizationCode: '',
    username: '',
    password: '',
    loading: false,
    formInvalid: false,
    loginFailed: false,
    invalidTenant: false,
    networkError: false,
  }

  _isFormValid() {
    if (
      !requiredStringValidator(this.state.username)
      || !passwordValidator(this.state.password)
      || !requiredStringValidator(this.state.organizationCode)
    ) {
      return false;
    }
    return true;
  }

  _onLoginPress() {
    this.setState({
      loading: true,
      formInvalid: false,
      loginFailed: false,
      invalidTenant: false,
      networkError: false,
    });
    if (!this._isFormValid()) {
      this.setState({
        loading: false,
        formInvalid: true,
      });
      return;
    }
    this.props.setOrganizationCodeAction(this.state.organizationCode);
    loginApi({
      email: this.state.username,
      password: this.state.password
    })
    .then((data) => {
      this.setState({
        loading: false,
      });
      const {
        token,
        ...user
      } = data;
      if (CONFIG.LOG) {
        console.log(`Registering on OneSignal with external id: ${user.domain}_${user.email}`);
      }
      OneSignal.setExternalUserId(`${user.domain}_${user.email}`);
      OneSignal.sendTags({
        name: user.name,
        role: 'student',
        org: user.domain,
        email: user.email,
      });
      this.props.setTokenAction(token);
      this.props.setUserDataAction(user);
      this.props.loginAction();
    })
    .catch((e) => {
      if (CONFIG.LOG) {
        console.log('Login Failed', e);
      }
      if (networkError(e)) {
        this.setState({
          networkError: true
        });
      }
      if (e.data && e.data.invalid_tenant) {
        this.setState({
          invalidTenant: true,
        });
      }
      this.setState({
        loading: false,
        loginFailed: true,
      });
    });
  }

  _renderErrorMessages() {
    if (this.state.formInvalid) {
      return (
        <Text style={styles.errorMessageStyle}>
          Please fill all fields correctly
        </Text>
      );
    }
    if (this.state.networkError) {
      return (
        <Text style={styles.errorMessageStyle}>
          Please check your internet connection
        </Text>
      );
    }
    if (this.state.invalidTenant) {
      return (
        <Text style={styles.errorMessageStyle}>
          Please check Organization Code
        </Text>
      );
    }
    if (this.state.loginFailed) {
      return (
        <Text style={styles.errorMessageStyle}>
          Invalid Username or Password
        </Text>
      );
    }
    if (this.props.tokenError) {
      return (
        <Text style={styles.errorMessageStyle}>
          Your Session Has Expired. Please Login Again
        </Text>
      );
    }
  }

  _renderLoaderOrLoginText() {
    if (this.state.loading) {
      return (
        <ActivityIndicator
          color={theme.COLORS.WHITE}
        />
      );
    }
    return (
      <Text style={styles.buttonTextStyle}>
        SIGN IN
      </Text>
    );
  }

  _renderCardTop() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        <View style={styles.logoContainerStyle}>
          <Image source={LOGO} style={styles.logoStyle} />
        </View>
        <Input
          style={styles.transparentInputStyle}
          placeholder={LANG.PLACEHOLDERS.organizationCode}
          color={theme.COLORS.ICON}
          returnKeyType='done'
          placeholderTextColor={MATERIAL_THEME.COLORS.MUTED}
          value={this.state.organizationCode}
          onChangeText={(organizationCode) => { this.setState({ organizationCode }); }}
        />
        <Input
          style={styles.transparentInputStyle}
          placeholder={LANG.PLACEHOLDERS.username}
          color={theme.COLORS.ICON}
          returnKeyType='done'
          placeholderTextColor={MATERIAL_THEME.COLORS.MUTED}
          value={this.state.username}
          onChangeText={(username) => { this.setState({ username }); }}
        />
        <Input
          style={styles.transparentInputStyle}
          password
          viewPass
          placeholder={LANG.PLACEHOLDERS.password}
          color={theme.COLORS.ICON}
          returnKeyType='done'
          placeholderTextColor={MATERIAL_THEME.COLORS.MUTED}
          value={this.state.password}
          onChangeText={(password) => { this.setState({ password }); }}
          iconColor={MATERIAL_THEME.COLORS.LIGHT_GREY}
        />
        <View
          style={styles.forgotPasswordContainerStyle}
        >
          <TouchableOpacity
            onPress={() => { this.props.navigation.navigate('InitiateForgotPassword'); }}
          >
            <Text style={styles.forgotPasswordTextStyle}>
              Unable to Login?
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  _renderCardBottom() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, marginTop: -(theme.SIZES.BASE * 2), marginBottom: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        <Button
          style={styles.buttonStyle}
          color={MATERIAL_THEME.COLORS.SUCCESS}
          onPress={() => { this._onLoginPress(); }}
          disabled={this.state.loading}
        >
          {this._renderLoaderOrLoginText()}
        </Button>
        {this._renderErrorMessages()}
      </View>
    );
  }

  _renderCardContents() {
    return (
      <View style={styles.cardContentContaier}>
        {this._renderCardTop()}
        <Image source={SLANT_STRIP} style={styles.slantStyle} />
        {this._renderCardBottom()}
      </View>
    );
  }

  _renderFooter() {
    return (
      <TouchableOpacity onPress={() => { Linking.openURL('http://sagokomm.com').catch((err) => CONFIG.LOG && console.error('An error occurred', err)); }}>
        <Block style={styles.bottomTextContainer} center>
          <Text style={styles.bottomText}>Powered By Sagokomm</Text>
        </Block>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND }}>
        <StatusBar backgroundColor={MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND} animated />
        <ScrollView contentContainerStyle={styles.containerStyle}>
          <View style={styles.card}>
            {this._renderCardContents()}
          </View>
        </ScrollView>
        {this._renderFooter()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    paddingTop: VALUES.iPhoneX ? theme.SIZES.BASE * 4 : theme.SIZES.BASE,
    backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND,
  },
  slantStyle: {
    width,
    height: width * 0.32,
    marginLeft: -(width / 10),
  },
  card: {
    margin: width / 10,
    backgroundColor: '#fff',
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 1,
    shadowOpacity: 0.6,
    elevation: 4,
  },
  bottomTextContainer: {
    marginBottom: 12,
  },
  bottomText: {
    color: theme.COLORS.WHITE
  },
  logoContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: theme.SIZES.BASE * 2,
  },
  logoStyle: {
    width: 100,
    height: 100,
  },
  cardContentContaier: {
    flex: 0,
    width: '80%',
  },
  transparentInputStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: MATERIAL_THEME.COLORS.LIGHT_GREY,
  },
  forgotPasswordContainerStyle: {
    alignSelf: 'flex-end',
    marginTop: theme.SIZES.BASE / 2,
    color: '#000',
  },
  forgotPasswordTextStyle: {
    color: theme.COLORS.ICON,
    marginTop: theme.SIZES.BASE * 2,
  },
  buttonStyle: {
    marginTop: theme.SIZES.BASE,
    width: '60%',
    alignSelf: 'center',
  },
  buttonTextStyle: {
    color: theme.COLORS.WHITE,
    fontSize: theme.SIZES.FONT,
  },
  errorMessageStyle: {
    alignSelf: 'center',
    marginTop: theme.SIZES.BASE,
    color: theme.COLORS.ERROR,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

const mapStateToProps = (state) => {
  const { tokenError } = state.Auth;
  return { tokenError };
};

export default connect(mapStateToProps, {
  loginAction,
  setUserDataAction,
  setTokenAction,
  setOrganizationCodeAction,
})(Login);
