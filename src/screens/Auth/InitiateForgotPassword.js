import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  Image,
  StyleSheet,
  ActivityIndicator,
  Dimensions,
  TouchableOpacity,
  Linking,
} from 'react-native';
import { connect } from 'react-redux';
import {
  theme,
  Input,
  Button,
} from 'galio-framework';
import { LOGO, SLANT_STRIP } from 'assets/images';
import { LANG, MATERIAL_THEME, VALUES, CONFIG } from 'constants';
import {
  requiredStringValidator,
} from 'validators';
import { setOrganizationCodeAction } from 'actions/AuthActions';
import { requestOtpApi } from 'api/OtpApi';

const { height, width } = Dimensions.get('window');

class InitiateForgotPassword extends Component {
  state = {
    organizationCode: '',
    username: '',
    loading: false,
    formInvalid: false,
    requestFailed: false,
    errorObject: null,
  }

  _isFormValid() {
    if (!requiredStringValidator(this.state.username)) {
      return false;
    }
    if (!requiredStringValidator(this.state.organizationCode)) {
      return false;
    }
    return true;
  }

  _onButtonPress() {
    this.setState({
      loading: true,
      formInvalid: false,
      requestFailed: false,
    });
    if (!this._isFormValid()) {
      this.setState({
        loading: false,
        formInvalid: true,
      });
      return;
    }
    this.props.setOrganizationCodeAction(this.state.organizationCode);
    requestOtpApi({
      email: this.state.username,
    })
    .then((data) => {
      this.setState({
        loading: false,
      });
      this.props.navigation.navigate('OtpVerification', {
        mobile_number: data.mobile_number,
        email: this.state.username,
      });
    })
    .catch((err) => {
      this.setState({
        loading: false,
        requestFailed: true,
        errorObject: err,
      });
    });
  }

  _renderErrorMessages() {
    const { errorObject } = this.state;
    if (this.state.formInvalid) {
      return (
        <Text style={styles.errorMessageStyle}>
          Please Enter Valid Username
        </Text>
      );
    }
    if (this.state.requestFailed) {
      return (
        <Text style={styles.errorMessageStyle}>
          {(errorObject && errorObject.data && errorObject.data.otp_reset_enable === false) ||
            (errorObject && errorObject.data && errorObject.data.insufficient_sms_credits) ?
            "Your organization has disabled OTPs. Please contact your organization's administration."
            :
            'Invalid Username'
          }
        </Text>
      );
    }
  }

  _renderLoaderOrText() {
    if (this.state.loading) {
      return (
        <ActivityIndicator
          color={theme.COLORS.WHITE}
        />
      );
    }
    return (
      <Text style={styles.buttonTextStyle}>
        REQUEST OTP
      </Text>
    );
  }

  _renderBack() {
    return (
      <View style={styles.backContainerStyle}>
        <Button
          style={styles.backButtonStyle}
          color={theme.COLORS.ICON}
          onPress={() => { this.props.navigation.goBack(); }}
          disabled={this.state.loading}
        >
          <Text style={{ color: theme.COLORS.WHITE }}>Back</Text>
        </Button>
        <View style={{ marginTop: -4, alignItems: 'center' }}>
          <Text style={{ color: theme.COLORS.ICON, fontSize: 20 }}>Forgot Password</Text>
        </View>
      </View>
    );
  }

  _renderCardTop() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        <View style={styles.logoContainerStyle}>
          <Image source={LOGO} style={styles.logoStyle} />
        </View>
        <Input
          style={styles.transparentInputStyle}
          placeholder={LANG.PLACEHOLDERS.organizationCode}
          color={theme.COLORS.ICON}
          returnKeyType='done'
          placeholderTextColor={MATERIAL_THEME.COLORS.MUTED}
          value={this.state.organizationCode}
          onChangeText={(organizationCode) => { this.setState({ organizationCode }); }}
        />
        <Input
          style={styles.transparentInputStyle}
          placeholder={LANG.PLACEHOLDERS.username}
          color={theme.COLORS.ICON}
          returnKeyType='done'
          placeholderTextColor={MATERIAL_THEME.COLORS.MUTED}
          value={this.state.username}
          onChangeText={(username) => { this.setState({ username }); }}
        />
      </View>
    );
  }

  _renderCardBottom() {
    return (
      <View style={{ marginLeft: theme.SIZES.BASE * 2, marginTop: -(theme.SIZES.BASE * 2), marginBottom: theme.SIZES.BASE * 2, width: width * 0.6 }}>
        <Button
          style={styles.buttonStyle}
          color={MATERIAL_THEME.COLORS.SUCCESS}
          onPress={() => { this._onButtonPress(); }}
          disabled={this.state.loading}
        >
          {this._renderLoaderOrText()}
        </Button>
        {this._renderErrorMessages()}
      </View>
    );
  }

  _renderCardContents() {
    return (
      <View style={styles.cardContentContaier}>
        {this._renderCardTop()}
        <Image source={SLANT_STRIP} style={styles.slantStyle} />
        {this._renderCardBottom()}
      </View>
    );
  }

  _renderFooter() {
    return (
      <TouchableOpacity onPress={() => { Linking.openURL('http://sagokomm.com').catch((err) => CONFIG.LOG && console.error('An error occurred', err)); }}>
        <View style={styles.bottomTextContainer}>
          <Text style={styles.bottomText}>Powered By Sagokomm</Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND }}>
        <ScrollView contentContainerStyle={styles.containerStyle}>
          {this._renderBack()}
          <View style={styles.card}>
            {this._renderCardContents()}
          </View>
        </ScrollView>
        {this._renderFooter()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND,
  },
  backContainerStyle: {
    paddingTop: VALUES.iPhoneX ? theme.SIZES.BASE * 5 : theme.SIZES.BASE * 2,
    marginBottom: theme.SIZES.BASE / 2,
  },
  slantStyle: {
    width,
    height: width * 0.32,
    marginLeft: -(width / 10),
  },
  card: {
    margin: width / 10,
    marginTop: (height - 580) / 2,
    backgroundColor: '#fff',
    borderRadius: 16,
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 1,
    shadowOpacity: 0.6,
    elevation: 4,
  },
  cardContentContaier: {
    flex: 0,
    width: '80%',
  },
  bottomTextContainer: {
    marginBottom: 12,
    alignItems: 'center',
  },
  bottomText: {
    color: theme.COLORS.WHITE,
  },
  logoContainerStyle: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: theme.SIZES.BASE * 2,
  },
  logoStyle: {
    width: 100,
    height: 100,
  },
  transparentInputStyle: {
    backgroundColor: 'transparent',
    borderWidth: 0,
    borderBottomWidth: 1,
    borderColor: MATERIAL_THEME.COLORS.LIGHT_GREY,
  },
  buttonStyle: {
    marginTop: theme.SIZES.BASE * 3,
    width: '60%',
    alignSelf: 'center',
  },
  buttonTextStyle: {
    fontSize: theme.SIZES.FONT,
    color: theme.COLORS.WHITE,
  },
  errorMessageStyle: {
    alignSelf: 'center',
    textAlign: 'center',
    marginTop: theme.SIZES.BASE,
    color: theme.COLORS.ERROR,
    fontWeight: 'bold',
  },
  backButtonStyle: {
    position: 'absolute',
    top: theme.SIZES.BASE * 1.5,
    left: theme.SIZES.BASE * 1.5,
    width: 60,
    height: 30,
    borderRadius: 15,
  },
});

export default connect(null, { setOrganizationCodeAction })(InitiateForgotPassword);
