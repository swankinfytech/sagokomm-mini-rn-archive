import React from 'react';
import { Dimensions, ScrollView, ImageBackground, Platform, StatusBar, View, Share, Animated, PermissionsAndroid, TouchableOpacity } from 'react-native';
import { Block, Text, theme, Button, Icon } from 'galio-framework';
import RNFetchBlob from 'rn-fetch-blob';
import { LinearGradient } from 'expo-linear-gradient';
import Hyperlink from 'react-native-hyperlink';
import { DownloadIndicator, ToastBox } from 'components';
import { IMG_BG1 } from 'assets/images';
import { playVideo, getVideoThumbnail } from 'utilities/Helpers';
import { MATERIAL_THEME } from 'constants';

const { width, height } = Dimensions.get('screen');
const thumbMeasure = (width - 48 - 32) / 3;

const AnimatedScrollView = Animated.createAnimatedComponent(ScrollView);
const AnimatedImageBackground = Animated.createAnimatedComponent(ImageBackground);
const StatusHeight = StatusBar.currentHeight;
const HeaderHeight = ((theme.SIZES.BASE * 3.5) + (StatusHeight || 0));
const dirs = RNFetchBlob.fs.dirs;
const android = RNFetchBlob.android;

export default class ViewAssignment extends React.Component {
  state = {
    downloadingAssets: [],
    downloadPercentages: [],
    errorBox: false,
    errorMessage: 'Some error occured',
    scrollY: new Animated.Value(0),
  }

  getAttachmentNumber(id) {
    const { assignment } = this.props.route.params;
    const attachments = [];
    assignment.assignment_assets.forEach(asset => {
      if (asset.type !== 0) attachments.push(asset.id);
    });
    const num = attachments.indexOf(id) + 1;
    return num;
  }

  initiateDownload(index, url) {
    if (Platform.OS === 'ios') {
      this.downloadFile(index, url);
      return;
    }
    PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
    .then((granted) => {
      if (!granted) {
        PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE)
        .then((result) => {
          if (result === PermissionsAndroid.RESULTS.GRANTED) {
            this.downloadFile(index, url);
          }
        })
        .catch((e) => {
          console.log(e);
          this.setState({ errorBox: true, errorMessage: 'File could not be downloaded' });
        });
      } else {
        this.downloadFile(index, url);
      }
    })
    .catch(() => {
      this.setState({ errorBox: true, errorMessage: 'File could not be downloaded' });
    });
  }

  downloadFile(index, url) {
    let downloadingAssets = [...this.state.downloadingAssets];
    const downloadPercentages = [...this.state.downloadPercentages];
    downloadPercentages[index] = 0;
    downloadingAssets.push(index);
    const path = Platform.OS === 'android' ? dirs.DownloadDir : dirs.DocumentDir;
    const name = url.split('/').pop();
    this.setState({ downloadingAssets, downloadPercentages });
    RNFetchBlob
    .config({
      fileCache: true,
      path: `${path}/${name}`,
    })
    .fetch('GET', url)
    .progress((received, total) => {
      downloadPercentages[index] = parseInt((received / total) * 100, 10);
      this.setState({ downloadPercentages });
    })
    .then((res) => {
      if (Platform.OS === 'android') {
        android.actionViewIntent(res.path(), res.respInfo.headers['Content-Type']);
      }
      if (Platform.OS === 'ios') {
        Share.share({ url: res.path() })
        .then(() => {
          res.flush();
        });
      }
      downloadingAssets = downloadingAssets.filter(asset => {
        return asset !== index;
      });
      downloadPercentages[index] = 0;
      this.setState({ downloadingAssets, downloadPercentages });
    })
    .catch((e) => {
      console.log(e);
      downloadingAssets = downloadingAssets.filter(asset => {
        return asset !== index;
      });
      downloadPercentages[index] = 0;
      this.setState({ downloadingAssets, downloadPercentages, errorBox: true, errorMessage: 'File could not be downloaded' });
    });
  }

  renderHeader = (assignment) => (
    <View>
      <View style={{ flexDirection: 'row' }}>
        <Text color="white" size={28} style={{ padding: 8 }}>{assignment.title} ({assignment.group_name})</Text>
      </View>
      <View style={{ flexDirection: 'row' }}>
        <Hyperlink linkDefault linkStyle={{ backgroundColor: 'rgba(0,0,0,0.6)', color: MATERIAL_THEME.COLORS.INFO }}>
          <Text size={16} style={styles.pro}>{assignment.message}</Text>
        </Hyperlink>
      </View>
    </View>
  )

  renderContent = (assignment, assets, videos) => (
    <View>
      <Block row space="between" style={{ paddingVertical: theme.SIZES.BASE, paddingHorizontal: theme.SIZES.BASE * 2 }}>
        <Block middle>
          <Text bold size={12} style={{ marginBottom: 8 }}>{assets}</Text>
          <Text muted size={12}>Attachment{assets === 1 ? '' : 's'}</Text>
        </Block>
        <Block middle>
          <Text bold size={12} style={{ marginBottom: 8 }}>{videos}</Text>
          <Text muted size={12}>Video{videos === 1 ? '' : 's'}</Text>
        </Block>
      </Block>
      <View style={{ marginTop: 16, paddingBottom: -HeaderHeight * 2 }}>
        <View>
          {assignment.assignment_assets.map((asset, index) => (
            <View key={index}>
              {
                asset.type === 0 ?
                <TouchableOpacity onPress={() => playVideo(asset.url, () => this.setState({ errorBox: true, errorMessage: 'This video could not be played' }))}>
                  <ImageBackground
                    source={{ uri: getVideoThumbnail(asset.url) }}
                    style={styles.videoThumbnail}
                    imageStyle={styles.imgThumbnail}
                  >
                    <View style={styles.thumbnailBackround}>
                      <Icon size={64} color="white" name="video-camera" family="entypo" />
                    </View>
                  </ImageBackground>
                </TouchableOpacity> :
                <Block center>
                  <Button
                    shadowless
                    disabled={this.state.downloadingAssets.indexOf(index) > -1}
                    style={[styles.button, styles.shadow]}
                    onPress={() => this.initiateDownload(index, asset.url)}
                    color={this.state.downloadingAssets.indexOf(index) > -1 ? 'grey' : 'primary'}
                  >
                    <Text color="white">Attachment #{this.getAttachmentNumber(asset.id)}</Text>
                    {
                      this.state.downloadingAssets.indexOf(index) > -1 ?
                      <DownloadIndicator
                        percentage={this.state.downloadPercentages[index]}
                        type='circle'
                        style={styles.documentDowloadIndicatorStyle}
                      /> :
                      <Icon
                        size={16}
                        color="white"
                        name="download"
                        family="entypo"
                      />
                    }
                  </Button>
                </Block>
              }
            </View>
          ))}
        </View>
      </View>
    </View>
  )

  render() {
    const { assignment } = this.props.route.params;
    let assets = 0;
    let videos = 0;
    assignment.assignment_assets.forEach((asset) => {
      if (asset.type === 0) {
        videos++;
      } else {
        assets++;
      }
    });
    const titleTransformY = this.state.scrollY.interpolate({
      inputRange: [0, height],
      outputRange: [0, height / 2],
      extrapolate: 'clamp',
    });

    return (
      <Block flex style={styles.profile}>
        <AnimatedScrollView
          onScroll={
            Animated.event(
              [{ nativeEvent: { contentOffset: { y: this.state.scrollY } } }],
              { useNativeDriver: true })
          }
          scrollEventThrottle={8}
        >
          <AnimatedImageBackground
            source={IMG_BG1}
            style={[
              styles.profileContainer,
              {
                  transform: [
                  { translateY: titleTransformY },
                ],
              },
            ]}
          >
            <View style={styles.profileDetails}>
              {this.renderHeader(assignment)}
              <LinearGradient colors={['rgba(0,0,0,0)', 'rgba(0,0,0,1)']} style={styles.gradient} />
            </View>
          </AnimatedImageBackground>
          <Block flex style={styles.options}>
            {this.renderContent(assignment, assets, videos)}
          </Block>
        </AnimatedScrollView>
        <ToastBox
          isShow={this.state.errorBox}
          color="warning"
          message={this.state.errorMessage}
          position="bottom"
          offset={40}
          duration={3000}
          onHide={() => this.setState({ errorBox: false })}
        />
      </Block>
    );
  }
}

const styles = {
  profile: {
    marginTop: Platform.OS === 'android' ? -HeaderHeight : 0,
  },
  profileContainer: {
    width,
  },
  profileDetails: {
    paddingTop: theme.SIZES.BASE * 8,
    paddingBottom: theme.SIZES.BASE * 5,
    paddingHorizontal: theme.SIZES.BASE,
    justifyContent: 'flex-start',
    flexGrow: 1,
  },
  pro: {
    backgroundColor: 'rgba(0,0,0,0.2)',
    paddingHorizontal: 6,
    borderRadius: 4,
    marginTop: 12,
    color: 'white',
  },
  options: {
    position: 'relative',
    padding: theme.SIZES.BASE,
    marginHorizontal: theme.SIZES.BASE,
    marginTop: -theme.SIZES.BASE * 3,
    borderTopLeftRadius: 13,
    borderTopRightRadius: 13,
    backgroundColor: theme.COLORS.WHITE,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    zIndex: 2,
  },
  thumb: {
    borderRadius: 4,
    marginVertical: 4,
    alignSelf: 'center',
    width: thumbMeasure,
    height: thumbMeasure
  },
  gradient: {
    zIndex: 1,
    left: 0,
    right: 0,
    bottom: 0,
    height: '30%',
    position: 'absolute',
  },
  shadow: {
    shadowColor: '#000000',
    shadowOffset: { width: 0, height: 4 },
    shadowRadius: 1,
    shadowOpacity: 0.6,
    elevation: 4,
  },
  button: {
    flexDirection: 'row',
    marginBottom: theme.SIZES.BASE,
    width: width - (theme.SIZES.BASE * 4),
    paddingHorizontal: 12,
    justifyContent: 'space-between',
  },
  documentDowloadIndicatorStyle: {
    size: 36,
    unfilledColor: '#fff',
    fill: '#808080',
    color: '#00BCD4',
    borderColor: '#808080',
  },
  videoThumbnail: {
    width: width - (theme.SIZES.BASE * 4),
    height: (width - (theme.SIZES.BASE * 4)) * 0.6,
    marginBottom: theme.SIZES.BASE,
    borderRadius: 10,
    overflow: 'hidden',
  },
  imgThumbnail: {
    width: width - (theme.SIZES.BASE),
    marginLeft: -theme.SIZES.BASE,
    height: (width - theme.SIZES.BASE) * 0.6,
    marginTop: -theme.SIZES.BASE,
  },
  thumbnailBackround: {
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    flexGrow: 1
  },
};
