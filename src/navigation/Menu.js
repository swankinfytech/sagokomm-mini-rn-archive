import React from 'react';
import { TouchableWithoutFeedback, ScrollView, StyleSheet, Image } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { useSafeArea } from 'react-native-safe-area-context';
import { useDispatch, useStore } from 'react-redux';
import { DrawerItem } from 'components';
import { MATERIAL_THEME, CONFIG } from 'constants';
import { logoutAction } from 'actions/AuthActions';
import { logoutApi } from 'api/AuthApi';
import { AVATAR } from 'assets/images';

function CustomDrawerContent({
  drawerPosition,
  navigation,
  state,
  // eslint-disable-next-line
  ...rest
}) {
  const insets = useSafeArea();
  const screens = [
    {
      title: 'Home',
      navigateTo: 'Home',
    },
    {
      title: 'Assignments',
      navigateTo: 'Assignments',
    },
    {
      title: 'Streams',
      navigateTo: 'Streams',
    },
    {
      title: 'Live Classes',
      navigateTo: 'Conferences',
    },
    {
      title: 'Feedback',
      navigateTo: 'Feedback',
    },
  ];
  const dispatch = useDispatch();
  const store = useStore();
  const storeData = store.getState();
  const user = storeData.User.user;

  return (
    <Block
      style={styles.container}
      forceInset={{ top: 'always', horizontal: 'never' }}
    >
      <Block flex={0.35} style={styles.header}>
        <TouchableWithoutFeedback
          onPress={() => {}}
        >
          <Block style={styles.avatarAndVersionContainer}>
            <Block style={styles.profile}>
              <Image source={AVATAR} style={styles.avatar} />
              <Text h5 color={'white'}>
                {user.name}
              </Text>
            </Block>
            <Block>
              <Text muted>
                V{CONFIG.APP_VERSION}
              </Text>
            </Block>
          </Block>
        </TouchableWithoutFeedback>
        <Block row>
          <Text size={16} color={MATERIAL_THEME.COLORS.WARNING} style={styles.seller}>
            {user.email}
          </Text>
          <Text size={16} muted>
            Student
          </Text>
        </Block>
        <Block row>
          <Text size={16} color={MATERIAL_THEME.COLORS.WHITE} style={styles.seller}>
            {user.organization_name}
          </Text>
        </Block>
      </Block>
      <Block flex style={{ paddingTop: 7, paddingLeft: 7, paddingRight: 14 }}>
        <ScrollView
          contentContainerStyle={[
            {
              paddingTop: insets.top * 0.4,
              paddingLeft: drawerPosition === 'left' ? insets.left : 0,
              paddingRight: drawerPosition === 'right' ? insets.right : 0
            }
          ]}
          showsVerticalScrollIndicator={false}
        >
          {
            screens.map((item, index) => (
              <DrawerItem
                title={item.title}
                navigateTo={item.navigateTo}
                key={index}
                navigation={navigation}
                focused={state.index === index}
              />
            ))
          }
        </ScrollView>
      </Block>
      <Block flex={0.2} style={{ paddingLeft: 7, paddingRight: 14 }}>
        <DrawerItem
          title="Logout"
          onPress={() => {
            _handleLogout(dispatch);
          }}
          style={{ marginTop: 'auto' }}
        />
      </Block>
    </Block>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    backgroundColor: '#4B1958',
    paddingHorizontal: 28,
    paddingBottom: theme.SIZES.BASE,
    paddingTop: theme.SIZES.BASE * 2,
    justifyContent: 'center',
  },
  footer: {
    paddingHorizontal: 28,
    justifyContent: 'flex-end'
  },
  profile: {
    marginBottom: theme.SIZES.BASE / 2,
  },
  avatar: {
    height: 40,
    width: 40,
    borderRadius: 20,
    marginBottom: theme.SIZES.BASE,
  },
  pro: {
    backgroundColor: MATERIAL_THEME.COLORS.LABEL,
    paddingHorizontal: 6,
    marginRight: 8,
    borderRadius: 4,
    height: 19,
    width: 38,
  },
  seller: {
    marginRight: 16,
  },
  avatarAndVersionContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});

const _handleLogout = (dispatch) => {
  logoutApi();
  dispatch(logoutAction());
};

export default CustomDrawerContent;
