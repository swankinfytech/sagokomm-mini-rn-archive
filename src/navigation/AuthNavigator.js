import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  Login,
  InitiateForgotPassword,
  OtpVerification,
  ResetPassword,
} from 'screens/Auth';

const Stack = createStackNavigator();

export default function AuthStack(props) {
  return (
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="InitiateForgotPassword" component={InitiateForgotPassword} />
      <Stack.Screen name="OtpVerification" component={OtpVerification} />
      <Stack.Screen name="ResetPassword" component={ResetPassword} />
    </Stack.Navigator>
  );
}
