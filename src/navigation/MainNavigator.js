/**
 * The Main Navigator of the application rendered once Auth is complete.
 * The heirarchy of the navigator is as follows:
 * At the top, a Drawer Navigator
 * Each Screen in the Drawer Navigator is a Stack Navigator
 * Each Stack then renders the screens that it needs to display.
 */

import React from 'react';
import { Dimensions } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import {
  Home,
  Streams,
  Assignments,
  Feedback,
  Conferences,
  ChangeDefaultPassword,
} from 'screens/Main';
import { ViewAssignment } from 'screens/Viewers';
import DrawerMenu from 'navigation/Menu';
import { Header, TitleBar } from 'components';
import { IMAGES, MATERIAL_THEME } from 'constants';

const { width } = Dimensions.get('screen');

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const profile = {
  avatar: IMAGES.Profile,
  name: 'Rachel Brown',
  type: 'Seller',
  plan: 'Pro',
  rating: 4.8
};

const HomeStack = (props) => {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="HomeIndex"
        component={Home}
        options={{
          header: ({ navigation, scene }) => (
            <Header
              title="Home"
              navigation={navigation}
              scene={scene}
            />
          )
        }}
      />
      <Stack.Screen
        name="ViewAssignment"
        component={ViewAssignment}
        options={{
          header: ({ navigation, scene }) => (
            <TitleBar
              title="View Assignment"
              navigation={navigation}
              scene={scene}
            />
          )
        }}
      />
    </Stack.Navigator>
  );
};

function AssignmentsStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Assignments"
        component={Assignments}
        options={{
          header: ({ navigation, scene }) => (
            <Header
              title="Assignments"
              navigation={navigation}
              scene={scene}
            />
          )
        }}
      />
      <Stack.Screen
        name="ViewAssignment"
        component={ViewAssignment}
        options={{
          header: ({ navigation, scene }) => (
            <TitleBar
              title="View Assignment"
              navigation={navigation}
              scene={scene}
            />
          )
        }}
      />
    </Stack.Navigator>
  );
}

function StreamsStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Streams"
        component={Streams}
        options={{
          header: ({ navigation, scene }) => (
            <Header
              title="Streams"
              navigation={navigation}
              scene={scene}
            />
          )
        }}
      />
    </Stack.Navigator>
  );
}

function ConferencesStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Conferences"
        component={Conferences}
        options={{
          header: ({ navigation, scene }) => (
            <Header
              title="Live Classes"
              navigation={navigation}
              scene={scene}
            />
          )
        }}
      />
    </Stack.Navigator>
  );
}

function FeedbackStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="screen">
      <Stack.Screen
        name="Feedback"
        component={Feedback}
        options={{
          header: ({ navigation, scene }) => (
            <Header
              title="Feedback"
              navigation={navigation}
              scene={scene}
            />
          )
        }}
      />
    </Stack.Navigator>
  );
}

function ChangeDefaultPasswordStack(props) {
  return (
    <Stack.Navigator mode="card" headerMode="none">
      <Stack.Screen
        name="ChangeDefaultPassword"
        component={ChangeDefaultPassword}
      />
    </Stack.Navigator>
  );
}

export default function MainStack(props) {
  return (
    <Drawer.Navigator
      style={{ flex: 1 }}
      drawerContent={props => (
        <DrawerMenu {...props} profile={profile} />
      )}
      drawerStyle={drawerStyle}
      drawerContentOptions={drawerContentOptions}
      initialRouteName="Home"
    >
      <Drawer.Screen
        name="Home"
        component={HomeStack}
      />
      <Drawer.Screen
        name="Assignments"
        component={AssignmentsStack}
      />
      <Drawer.Screen
        name="Streams"
        component={StreamsStack}
      />
      <Drawer.Screen
        name="Conferences"
        component={ConferencesStack}
      />
      <Drawer.Screen
        name="Feedback"
        component={FeedbackStack}
      />
      <Drawer.Screen
        name="ChangeDefaultPassword"
        component={ChangeDefaultPasswordStack}
      />
    </Drawer.Navigator>
  );
}

const drawerContentOptions = {
  activeTintColor: 'white',
  inactiveTintColor: '#000',
  activeBackgroundColor: MATERIAL_THEME.COLORS.ACTIVE,
  inactiveBackgroundColor: 'transparent',
  itemStyle: {
    width: width * 0.74,
    paddingHorizontal: 12,
    // paddingVertical: 4,
    justifyContent: 'center',
    alignContent: 'center',
    // alignItems: 'center',
    overflow: 'hidden'
  },
  labelStyle: {
    fontSize: 18,
    fontWeight: 'normal'
  }
};

const drawerStyle = {
  backgroundColor: 'white',
  width: width * 0.8
};
