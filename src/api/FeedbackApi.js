import axios from 'axios';
import { PROTOCOL, MAIN_DOMAIN } from 'constants/Config';

export function sendFeedbackApi(data) {
  return axios.post(`${PROTOCOL}://${MAIN_DOMAIN}/api/v1/system_admin/feedback`, data);
}
