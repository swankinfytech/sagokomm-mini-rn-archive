import { GET } from 'utilities/AxiosHelpers';
import { PROTOCOL, MAIN_DOMAIN } from 'constants/Config';

export const getVersionApi = () => {
  return GET(
    `${PROTOCOL}://${MAIN_DOMAIN}/api/v1/system_admin/version`,
    {
      fqdnPassed: true
    }
  );
};

export const getMigrationStatusApi = () => {
  return GET(
    `${PROTOCOL}://${MAIN_DOMAIN}/api/v1/system_admin/migrate`,
    {
      fqdnPassed: true
    }
  );
};
