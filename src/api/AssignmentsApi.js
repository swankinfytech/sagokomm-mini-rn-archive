import { GET } from 'utilities/AxiosHelpers';

export const getAssignmentsApi = () => {
  return GET('assignments');
};
