import { GET } from 'utilities/AxiosHelpers';

export const getConferencesApi = () => {
  return GET('conferences');
};
