import { GET } from 'utilities/AxiosHelpers';

export const getDashApi = () => {
  return GET('dashboard');
};
