import { GET } from 'utilities/AxiosHelpers';

export const getStreamsApi = () => {
  return GET('streams');
};
