import { POST } from 'utilities/AxiosHelpers';

export const loginApi = (data) => {
  return POST('login', data);
};

export const logoutApi = () => {
  return POST('logout');
};

export const refreshTokenApi = () => {
  return POST('refresh');
};

export const initiateGuestPasswordResetApi = (data) => {
  return POST('forgot/initiate', data);
};

export const resetGuestPasswordApi = (data) => {
  return POST('forgot/change_password', data);
};

export const changeDefaultPasswordApi = (data) => {
  return POST('change_default_password', data);
};
