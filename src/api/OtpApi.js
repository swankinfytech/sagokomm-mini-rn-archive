import { POST } from 'utilities/AxiosHelpers';

export const requestOtpApi = (data) => {
  return POST('otp/send', data);
};

export const verifyOtpApi = (data) => {
  return POST('otp/verify', data);
};
