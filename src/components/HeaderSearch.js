import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
} from 'react-native';
import {
  Block,
  Input,
  theme,
} from 'galio-framework';
import Icon from './Icon';

const { width } = Dimensions.get('screen');

class HeaderSearch extends Component {
  render() {
    const {
      containerStyle,
    } = this.props;
    const { navigation } = this.props;
    return (
      <Block
        style={{
          ...styles.searchContainer,
          ...containerStyle
        }}
      >
        <Input
          right
          color="black"
          style={styles.search}
          placeholder="What are you looking for?"
          onFocus={() => navigation.navigate('Pro')}
          iconContent={<Icon size={16} color={theme.COLORS.MUTED} name="magnifying-glass" family="entypo" />}
        />
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  searchContainer: {
    width,
    backgroundColor: theme.COLORS.WHITE,
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
});

export { HeaderSearch };
