import React from 'react';
import { View, Dimensions, TouchableOpacity } from 'react-native';
import { Text, theme } from 'galio-framework';
import { formatDateTime } from 'utilities/Helpers';
import { MATERIAL_THEME } from 'constants';

const { width } = Dimensions.get('screen');

class AssignmentCard extends React.Component {
  render() {
    const { assignment, wide } = this.props;
    const customWidth = wide ? width - (theme.SIZES.BASE * 2) : null;

    return (
      <TouchableOpacity style={{ ...cardStyle, width: customWidth }} onPress={() => this.props.navigation.push('ViewAssignment', { assignment })}>
        <View style={{ marginVertical: 5, marginHorizontal: 12, flexShrink: 1 }}>
          <View style={{ margin: 8 }}>
            <Text bold size={textSize} style={textStyle}>{assignment.title}</Text>
            <Text muted>{formatDateTime(assignment.created_at)}</Text>
            <View style={{ flexDirection: 'row', flexShrink: 1 }}>
              <View style={groupBadge}>
                <Text color="#fff">{assignment.group_name}</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{ flexGrow: 1 }} />
        <View style={ovalStyle}>
          <Text size={20} color="#fff">{assignment.assignment_assets.length}</Text>
          <Text size={12} color="#fff">attachment{assignment.assignment_assets.length === 1 ? '' : 's'}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export default AssignmentCard;

const textSize = 16;

const cardStyle = {
  flex: 1,
  flexDirection: 'row',
  marginVertical: 10,
  marginHorizontal: 12,
  borderRadius: 8,
  backgroundColor: theme.COLORS.WHITE,
  shadowColor: theme.COLORS.BLACK,
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowRadius: 8,
  shadowOpacity: 0.2,
  elevation: 4,
  zIndex: 2,
  overflow: 'hidden',
};

const ovalStyle = {
  width: 64,
  flexGrow: 1,
  backgroundColor: theme.COLORS.FACEBOOK,
  borderTopLeftRadius: 60,
  borderBottomLeftRadius: 60,
  justifyContent: 'center',
  alignItems: 'center',
};

const groupBadge = {
  marginTop: 4,
  paddingVertical: 4,
  paddingHorizontal: 8,
  backgroundColor: MATERIAL_THEME.COLORS.BUTTON_COLOR,
  borderRadius: 12,
};

const textStyle = {
  marginBottom: 3,
};
