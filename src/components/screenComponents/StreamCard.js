import React from 'react';
import { Dimensions, TouchableOpacity, TouchableWithoutFeedback, Image, View } from 'react-native';
import { Text, theme, Block } from 'galio-framework';
import { CONFIG } from 'constants';
import { formatDateTime, playVideo, stringTruncate, getVideoThumbnail } from 'utilities/Helpers';

const { width } = Dimensions.get('screen');

class StreamCard extends React.Component {
  render() {
    const { stream } = this.props;
    const desc = stream.description && stringTruncate(stream.description, CONFIG.STRING_SLICING_LENGTH);
    const disabled = this.props.disabled ? this.props.disabled : false;

    return (
      <Block row card flex style={[styles.product, styles.shadow]}>
        <TouchableWithoutFeedback disabled={disabled} onPress={() => playVideo(stream.url, () => this.props.onError())}>
          <View style={styles.imageContainer}>
            <Image source={{ uri: getVideoThumbnail(stream.url) }} style={styles.image} />
          </View>
        </TouchableWithoutFeedback>
        <TouchableOpacity disabled={disabled} onPress={() => playVideo(stream.url, () => this.props.onError())}>
          <View style={styles.productDescription}>
            <Text size={14} style={styles.productTitle}>{stream.title}</Text>
            <Text size={12} style={styles.productTitle}>{desc}</Text>
            <View style={styles.time}>
              <Text size={10} muted>{formatDateTime(stream.scheduled_at)}</Text>
            </View>
          </View>
        </TouchableOpacity>
      </Block>
    );
  }
}

export default StreamCard;

const styles = {
  product: {
    backgroundColor: theme.COLORS.WHITE,
    marginVertical: theme.SIZES.BASE,
    borderWidth: 0,
    minHeight: 114,
    width: width - (theme.SIZES.BASE * 2),
  },
  productTitle: {
    flex: 0,
    flexWrap: 'wrap',
    paddingTop: 6,
    paddingLeft: 6,
  },
  productDescription: {
    flex: 0,
    flexDirection: 'column',
    flexGrow: 1,
    padding: theme.SIZES.BASE / 2,
    width: ((width * 1.7) / 2.7) - (theme.SIZES.BASE * 3),
  },
  time: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    paddingRight: 12,
  },
  imageContainer: {
    elevation: 1,
    marginTop: -16,
    marginLeft: theme.SIZES.BASE / 2,
    height: width * 0.3,
    width: width * 0.4,
    borderRadius: 8,
    overflow: 'hidden',
  },
  image: {
    height: width * 0.5,
    marginTop: -(width * 0.1),
    width: width * 0.45,
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowRadius: 8,
    shadowOpacity: 0.2,
    elevation: 4,
  },
};
