import React from 'react';
import { TouchableOpacity, Image, View, Dimensions } from 'react-native';
import { Text, theme } from 'galio-framework';
import { LinearGradient } from 'expo-linear-gradient';
import { formatShortDateTime, secondsFormatter } from 'utilities/Helpers';
import { ZOOM_LOGO } from 'assets/images';

const { width } = Dimensions.get('screen');

class ConferenceCard extends React.Component {
  _handlePress() {
    if (this.props.onPress && !this.props.disabled) {
      this.props.onPress({
        conference: this.props.conference
      });
    }
  }

  render() {
    const { conference, wide } = this.props;
    let groups = [];
    conference.groups.forEach((group) => {
      groups.push(group.name);
    });
    groups = groups.join(', ');

    return (
      <TouchableOpacity style={cardStyle(wide)} onPress={() => { this._handlePress(); }}>
        <View style={{ marginVertical: 5, marginHorizontal: 12, flexShrink: 1 }}>
          <View style={{ margin: 8 }}>
            <Text bold size={textSize} style={textStyle}>{conference.title}</Text>
            <Text muted style={{ marginBottom: 8 }}>{groups}</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginRight: -12 }}>
              <Text muted>{formatShortDateTime(conference.scheduled_at)}</Text>
              <Text muted>{secondsFormatter(conference.duration)}</Text>
            </View>
          </View>
        </View>
        <LinearGradient colors={['#57a3ff', '#3d83fc']} style={ovalStyle}>
          <Image source={ZOOM_LOGO} style={getHeightWidth(55)} />
        </LinearGradient>
      </TouchableOpacity>
    );
  }
}

export default ConferenceCard;

const textSize = 16;

const getHeightWidth = (desiredWidth) => ({
  width: desiredWidth,
  height: (desiredWidth * 639) / 1060,
});

const cardStyle = (wide) => ({
  flex: 1,
  width: width - (theme.SIZES.BASE * 1.5),
  marginLeft: wide ? 12 : 1,
  flexDirection: 'row',
  justifyContent: 'space-between',
  marginVertical: 10,
  borderRadius: 8,
  backgroundColor: theme.COLORS.WHITE,
  shadowColor: theme.COLORS.BLACK,
  shadowOffset: {
    width: 0,
    height: 4,
  },
  shadowRadius: 8,
  shadowOpacity: 0.2,
  elevation: 4,
  zIndex: 2,
  overflow: 'hidden',
});

const ovalStyle = {
  height: 'auto',
  width: 120,
  marginRight: -10,
  borderTopLeftRadius: 60,
  borderBottomLeftRadius: 60,
  justifyContent: 'center',
  alignItems: 'center',
};

const textStyle = {
  marginBottom: 3,
};
