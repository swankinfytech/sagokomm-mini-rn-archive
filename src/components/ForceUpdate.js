import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, Linking, Dimensions, StatusBar } from 'react-native';
import { Icon } from 'galio-framework';
import { MATERIAL_THEME } from 'constants';

// TODO: add iOS app store link

const { width } = Dimensions.get('window');

export class ForceUpdate extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center' }}>
        <StatusBar backgroundColor={MATERIAL_THEME.COLORS.FACEBOOK} />
        <View>
          <View style={{ width, height: 64, backgroundColor: MATERIAL_THEME.COLORS.MUTED, marginTop: 32 }} />
          <View style={topSecondarySlant} />
          <View style={{ width, height: 64, backgroundColor: MATERIAL_THEME.COLORS.FACEBOOK, marginTop: -160 }} />
          <View style={topSlant} />
        </View>
        <View>
          <Text style={{ textAlign: 'center', fontSize: 20, color: '#000', marginBottom: 20, alignSelf: 'center', padding: 10, borderRadius: 10 }}>
            There is a new version of the app available. Please update to continue.
          </Text>
          <View style={{ alignItems: 'center' }}>
            {
              (Platform.OS === 'android') ?
              <TouchableOpacity
                style={{ backgroundColor: '#5E9F22', flexDirection: 'row', padding: 10, borderRadius: 10 }}
                onPress={() => Linking.openURL('https://play.google.com/store/apps/details?id=com.swankinfytech.sagokommmini')}
              >
                <Icon family="FontAwesome" name="android" style={{ fontSize: 30, color: '#fff' }} />
                <Text style={{ alignSelf: 'center', marginLeft: 10, fontSize: 16, color: '#fff' }}>Go to Play Store</Text>
              </TouchableOpacity> :
              <TouchableOpacity
                style={{ backgroundColor: '#000', flexDirection: 'row', padding: 10, borderRadius: 10 }}
                onPress={() => Linking.openURL('https://apps.apple.com/link-for-ios-app')}
              >
                <Icon family="AntDesign" name="apple1" style={{ fontSize: 30, color: '#fff' }} />
                <Text style={{ alignSelf: 'center', marginLeft: 10, fontSize: 16, color: '#fff' }}>Go to App Store</Text>
              </TouchableOpacity>
            }
          </View>
        </View>
        <View>
          <View style={bottomSlant} />
          <View style={{ width, height: 32, backgroundColor: MATERIAL_THEME.COLORS.FACEBOOK }} />
        </View>
      </View>
    );
  }
}

const topSlant = {
  width,
  height: 64,
  borderTopWidth: 32,
  borderBottomWidth: 32,
  borderTopColor: MATERIAL_THEME.COLORS.FACEBOOK,
  borderBottomColor: 'rgba(0,0,0,0)',
  borderLeftWidth: width / 2,
  borderRightWidth: width / 2,
  borderLeftColor: MATERIAL_THEME.COLORS.FACEBOOK,
  borderRightColor: 'rgba(0,0,0,0)',
};

const topSecondarySlant = {
  width,
  height: 64,
  borderTopWidth: 32,
  borderBottomWidth: 32,
  borderTopColor: MATERIAL_THEME.COLORS.MUTED,
  borderBottomColor: 'rgba(0,0,0,0)',
  borderLeftWidth: width / 2,
  borderRightWidth: width / 2,
  borderLeftColor: MATERIAL_THEME.COLORS.MUTED,
  borderRightColor: 'rgba(0,0,0,0)',
};

const bottomSlant = {
  width,
  height: 64,
  borderTopWidth: 32,
  borderBottomWidth: 32,
  borderTopColor: 'rgba(0,0,0,0)',
  borderBottomColor: MATERIAL_THEME.COLORS.FACEBOOK,
  borderLeftWidth: width / 2,
  borderRightWidth: width / 2,
  borderLeftColor: 'rgba(0,0,0,0)',
  borderRightColor: MATERIAL_THEME.COLORS.FACEBOOK,
};
