import React, { Component } from 'react';
import { StatusBar, View, Animated, Easing } from 'react-native';
import { MATERIAL_THEME } from 'constants';
import { ICON, LOADER_BG } from 'assets/images';

const logoSide = 140;
const ringSide = 200;
const moveLogo = (ringSide / 2) + 2;
const moveRing = (logoSide / 2) * -1;

export class SplashScreen extends Component {
  state = { scale: new Animated.Value(1) };

  componentDidMount() {
		this.animation1();
	}

	animation1() {
		Animated.timing(this.state.scale, {
			toValue: 1.1,
			easing: Easing.back(),
			duration: 1500,
			useNativeDriver: true,
		}).start(() => this.animation2());
	}

	animation2() {
		Animated.timing(this.state.scale, {
			toValue: 1,
			easing: Easing.back(),
			duration: 1500,
			useNativeDriver: true,
		}).start(() => this.animation1());
	}

  render() {
    return (
      <View style={styles.bgMainIntro}>
        <StatusBar backgroundColor={MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND} animated />
        <View style={styles.layoutInner}>
          <View style={styles.loginBg}>
            <Animated.Image
              source={ICON}
              style={{
                width: logoSide,
                height: logoSide,
                transform: [
                  { translateY: moveLogo },
                ],
                resizeMode: 'contain',
              }}
            />
            <Animated.Image
              source={LOADER_BG}
              style={{
                width: ringSide,
                height: ringSide,
                transform: [
                  { translateY: moveRing },
                  { scale: this.state.scale },
                ],
                resizeMode: 'contain',
              }}
            />
          </View>
        </View>
      </View>
    );
  }
}

const styles = {
  layoutContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: '100%',
  },
  loginBg: {
    flex: 1,
    width: '100%',
    justifyContent: 'center',
    paddingHorizontal: 30,
    alignItems: 'center',
  },
  bgMainIntro: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND,
    borderBottomWidth: 0,
  },
  layoutInner: {
    width: '100%',
    backgroundColor: MATERIAL_THEME.COLORS.CUSTOM_BACKGROUND,
  },
};
