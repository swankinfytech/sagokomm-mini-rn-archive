import React from 'react';
import { ActivityIndicator } from 'react-native';
import { Block } from 'galio-framework';

class Loader extends React.Component {
  render() {
    return (
      <Block flex space="around">
        <ActivityIndicator size={64} />
      </Block>
    );
  }
}

export default Loader;
