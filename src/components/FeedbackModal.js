import React from 'react';
import { View, Text, Modal, Linking } from 'react-native';
import { Button } from 'galio-framework';

export default class FeedbackModal extends React.Component {
  goToStore() {
    this.props.dismissModal();
    Linking.openURL('market://details?id=com.swankinfytech.sagokommmini')
    .catch(() => this.props.error());
  }

  render() {
    const { visible, dismissModal } = this.props;

    return (
      <Modal
        visible={visible}
        animated
        transparent
        animationType="fade"
        onDismiss={() => dismissModal()}
      >
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(0,0,0,0.6)' }}>
          <View style={{ backgroundColor: '#fff', padding: 16, borderRadius: 8, justifyContent: 'space-between', width: '80%', height: 240 }}>
            <Text style={{ fontSize: 20 }}>Please share your feedback on the Play Store?</Text>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Button style={{ width: '45%' }} onPress={() => this.goToStore()}>Go to Store</Button>
              <Button style={{ width: '45%' }} onPress={() => dismissModal()} color="#ccc">skip</Button>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
