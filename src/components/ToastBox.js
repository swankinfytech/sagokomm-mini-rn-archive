import React from 'react';
import { Toast } from 'galio-framework';

export default class ToastBox extends React.Component {
  getSnapshotBeforeUpdate(prevProps) {
    if (!prevProps.isShow) {
      setTimeout(() => {
        this.props.onHide();
      }, this.props.duration);
    }
  }

  render() {
    const { isShow, message, color, offset, position } = this.props;

    return (
      <Toast
        isShow={isShow}
        positionIndicator={position}
        positionOffset={offset}
        color={color}
      >
        {message}
      </Toast>
    );
  }
}
