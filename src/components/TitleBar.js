import React from 'react';
import { withNavigation } from '@react-navigation/compat';
import { View, TouchableOpacity } from 'react-native';
import { Block, Icon, Text } from 'galio-framework';

class TitleBar extends React.Component {
  handleLeftPress = () => {
    this.props.navigation.goBack();
  }

  render() {
    const { title } = this.props;

    return (
      <Block>
        <View style={{ flexDirection: 'row', backgroundColor: '#fff' }}>
          <TouchableOpacity onPress={this.handleLeftPress} style={{ paddingBottom: 6, paddingTop: 10, paddingHorizontal: 8 }}>
            <Icon
              name="chevron-left"
              family="entypo"
              size={30}
            />
          </TouchableOpacity>
          <Text size={20} bold style={{ paddingBottom: 6, paddingTop: 12 }}>{title}</Text>
        </View>
      </Block>
    );
  }
}

export default withNavigation(TitleBar);
