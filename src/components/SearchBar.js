import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
} from 'react-native';
import {
  Block,
  Input,
  theme,
} from 'galio-framework';
import Icon from './Icon';

const { width } = Dimensions.get('screen');

class SearchBar extends Component {
  render() {
    const {
      containerStyle,
    } = this.props;
    return (
      <Block
        style={{
          ...styles.searchContainer,
          ...containerStyle
        }}
      >
        <Input
          right
          color="black"
          style={styles.search}
          placeholder={this.props.placeholder ? this.props.placeholder : 'What are you looking for?'}
          value={this.props.value ? this.props.value : ''}
          onChangeText={(val) => this.props.onChangeText(val)}
          iconContent={<Icon size={16} color={theme.COLORS.MUTED} name="magnifying-glass" family="entypo" />}
        />
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  searchContainer: {
    width,
  },
  search: {
    height: 48,
    width: width - 32,
    marginHorizontal: 16,
    borderWidth: 1,
    borderRadius: 3,
  },
});

export { SearchBar };
