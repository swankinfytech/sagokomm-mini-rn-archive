import React from 'react';
import { View } from 'react-native';
import { Block, Text, Icon } from 'galio-framework';

class ErrorBox extends React.Component {
  render() {
    return (
      <Block flex space="around">
        <View style={{ alignItems: 'center' }}>
          <Icon
            size={96}
            name="error-outline"
            family="material-icons"
            color="red"
          />
          <Text h5 muted>Some error occured</Text>
          <Text h6 muted>(pull down to refresh)</Text>
        </View>
      </Block>
    );
  }
}

export default ErrorBox;
