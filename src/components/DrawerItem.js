import React from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import { Block, Text, theme } from 'galio-framework';
import { MATERIAL_THEME } from 'constants';

import Icon from './Icon';

class DrawerItem extends React.Component {
  _handleOnPress() {
    const { onPress, title, navigation, navigateTo } = this.props;
    if (onPress) {
      onPress();
    } else {
      navigation.navigate(navigateTo || title);
    }
  }

  renderIcon = () => {
    const { title, focused } = this.props;
    switch (title) {
      case 'Home':
        return (
          <Icon
            size={24}
            name="shop"
            family="entypo"
            color={focused ? 'white' : MATERIAL_THEME.COLORS.MUTED}
          />
        );
      case 'Assignments':
        return (
          <Icon
            size={24}
            name="book"
            family="foundation"
            color={focused ? 'white' : MATERIAL_THEME.COLORS.MUTED}
          />
        );
      case 'Streams':
        return (
          <Icon
            size={24}
            name="video"
            family="entypo"
            color={focused ? 'white' : MATERIAL_THEME.COLORS.MUTED}
          />
        );
      case 'Feedback':
        return (
          <Icon
            size={24}
            name="md-megaphone"
            family="ionicon"
            color={focused ? 'white' : MATERIAL_THEME.COLORS.MUTED}
          />
        );
      case 'Live Classes':
        return (
          <Icon
            size={24}
            name="group"
            family="fontAwesome"
            color={focused ? 'white' : MATERIAL_THEME.COLORS.MUTED}
          />
        );
      case 'Logout':
        return (
          <Icon
            size={24}
            name="ios-log-out"
            family="ionicon"
            color={focused ? 'white' : MATERIAL_THEME.COLORS.MUTED}
          />
        );
      case 'Sign Up':
        return (
          <Icon
            size={24}
            name="md-person-add"
            family="ionicon"
            color={focused ? 'white' : MATERIAL_THEME.COLORS.MUTED}
          />
        );
      default:
        return null;
    }
  };

  render() {
    const { focused, title, style } = this.props;
    return (
      <TouchableOpacity style={{ height: 55, ...style }} onPress={() => { this._handleOnPress(); }}>
        <Block
          flex
          row
          style={[
            styles.defaultStyle,
            focused ? [styles.activeStyle, styles.shadow] : null
          ]}
        >
          <Block middle flex={0.1} style={{ marginRight: 28 }}>
            {this.renderIcon()}
          </Block>
          <Block row center flex={0.9}>
            <Text
              size={18}
              color={
                focused
                  ? 'white'
                  : 'black'
              }
            >
              {title}
            </Text>
          </Block>
        </Block>
      </TouchableOpacity>
    );
  }
}

export default DrawerItem;

const styles = StyleSheet.create({
  defaultStyle: {
    paddingVertical: 16,
    paddingHorizontal: 16
  },
  activeStyle: {
    backgroundColor: MATERIAL_THEME.COLORS.ACTIVE,
    borderRadius: 4
  },
  shadow: {
    shadowColor: theme.COLORS.BLACK,
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 8,
    shadowOpacity: 0.2
  },
  pro: {
    backgroundColor: MATERIAL_THEME.COLORS.LABEL,
    paddingHorizontal: 6,
    marginLeft: 8,
    borderRadius: 2,
    height: 16,
    width: 36
  }
});
