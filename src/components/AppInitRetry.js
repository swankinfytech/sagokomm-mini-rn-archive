import React, { Component } from 'react';
import { View, Text, Image, Dimensions, StatusBar } from 'react-native';
import { Button } from 'galio-framework';
import { MATERIAL_THEME } from 'constants';
import { SATELLITE } from 'assets/images';
import { scaleImageSize } from 'utilities/Helpers';

// TODO: add iOS app store link

const { width } = Dimensions.get('window');
const scaledImageSize = scaleImageSize({
  originalWidth: 205,
  originalHeight: 171,
  scaledWidth: width * 0.4,
});

export class AppInitRetry extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <StatusBar barStyle='default' />
        <Image
          style={{ width: scaledImageSize.scaledWidth, height: scaledImageSize.scaledHeight }}
          source={SATELLITE}
        />
        <Text
          style={{
            fontWeight: 'bold',
            fontSize: 16,
            color: '#1a1ad5',
            marginTop: MATERIAL_THEME.SIZES.BASE * 3,
            marginBottom: MATERIAL_THEME.SIZES.BASE / 2,
          }}
        >
          Whoops!
        </Text>
        <Text
          style={{
            color: '#1e1e1e',
            marginTop: 5,
            textAlign: 'center',
          }}
        >
          There seems to be an issue with your connection
        </Text>
        <Button
          style={styles.retryButtonStyle}
          color={MATERIAL_THEME.COLORS.SUCCESS}
          onPress={() => { this.props.onRetry && this.props.onRetry(); }}
        >
          <Text style={styles.retryButtonTextStyle}>
            RETRY
          </Text>
        </Button>
        <Text
          style={{
            color: '#1e1e1e',
            marginVertical: MATERIAL_THEME.SIZES.BASE,
            textAlign: 'center',
          }}
        >
          --- OR ---
        </Text>
        <Button
          style={styles.signInButtonStyle}
          color={MATERIAL_THEME.COLORS.MUTED}
          onPress={() => { this.props.onSignInAgain && this.props.onSignInAgain(); }}
        >
          <Text style={styles.signInButtonTextStyle}>
            Try Logging In Again
          </Text>
        </Button>
      </View>
    );
  }
}

const styles = {
  retryButtonStyle: {
    marginTop: MATERIAL_THEME.SIZES.BASE * 2,
    width: '60%',
    alignSelf: 'center',
  },
  retryButtonTextStyle: {
    color: MATERIAL_THEME.COLORS.WHITE,
    fontSize: MATERIAL_THEME.SIZES.FONT,
  },
  signInButtonStyle: {
    // marginTop: MATERIAL_THEME.SIZES.BASE * 2,
    width: '60%',
    alignSelf: 'center',
  },
  signInButtonTextStyle: {
    color: MATERIAL_THEME.COLORS.WHITE,
    fontSize: MATERIAL_THEME.SIZES.FONT,
  },
};
