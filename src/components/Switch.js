import React from 'react';
import { Switch, Platform } from 'react-native';

import { MATERIAL_THEME } from 'constants';

export default class MkSwitch extends React.Component {
  render() {
    const { value, ...props } = this.props;
    const thumbColor = Platform.OS === 'ios' ? null :
      Platform.OS === 'android' && value ? MATERIAL_THEME.COLORS.SWITCH_ON : MATERIAL_THEME.COLORS.SWITCH_OFF;

    return (
      <Switch
        value={value}
        thumbColor={thumbColor}
        ios_backgroundColor={MATERIAL_THEME.COLORS.SWITCH_OFF}
        trackColor={{ false: MATERIAL_THEME.COLORS.SWITCH_OFF, true: MATERIAL_THEME.COLORS.SWITCH_ON }}
        {...props}
      />
    );
  }
}
