import React from 'react';
import { StyleSheet, View } from 'react-native';
import ModalDropdown from 'react-native-modal-dropdown';
import { Block, Text, Icon, theme } from 'galio-framework';

export default class DropDown extends React.Component {
  handleOnSelect = (index, value) => {
  }

  render() {
    const { onSelect, style, ...props } = this.props;

    return (
      <ModalDropdown
        style={[styles.qty, style]}
        onSelect={this.handleOnSelect}
        dropdownStyle={styles.dropdown}
        dropdownTextStyle={{ paddingHorizontal: 16, fontSize: 16 }}
        onSelect={(index, value) => this.props.onSelect(index, value)}
        renderSeparator={() => (<View />)}
        {...props}
      >
        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
          <Text size={16}>{this.props.value === '' ? 'Select type' : this.props.value}</Text>
          <Icon name="angle-down" family="font-awesome" size={16} />
        </View>
      </ModalDropdown>
    );
  }
}

const styles = StyleSheet.create({
  qty: {
    width: 160,
    backgroundColor: '#DCDCDC',
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 3,
    shadowColor: 'rgba(0, 0, 0, 0.1)',
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 4,
    shadowOpacity: 1,
  },
  dropdown: {
    marginTop: 8,
    marginLeft: -16,
    width: 160,
  },
});
