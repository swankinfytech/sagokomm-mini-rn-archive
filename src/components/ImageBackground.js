import React from 'react';
import {
  StyleSheet,
  Dimensions,
  ImageBackground as RNImageBackground,
} from 'react-native';

const { height, width } = Dimensions.get('window');

/**
 * Reusable component to render screen with splash screen background as background image
 * @param {object} props
 * @return {React.Component}
 */
export const ImageBackground = (props) => {
  return (
    <RNImageBackground
      source={props.source}
      style={{ ...styles.backgroundImageStyle, ...props.style }}
      {...props}
    >
      {props.children}
    </RNImageBackground>
  );
};

const styles = StyleSheet.create({
  backgroundImageStyle: {
    width,
    height,
  },
});
