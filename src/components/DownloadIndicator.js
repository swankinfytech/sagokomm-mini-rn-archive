import React, { Component } from 'react';
import { View } from 'react-native';
import * as Progress from 'react-native-progress';

class DownloadIndicatorClass extends Component {
  render() {
    const {
      percentage = 0,
      indeterminate = false,
      showsText = false,
      formatText = () => {},
      type = 'circle',
      style = {},
    } = this.props;
    const {
      size = 40,
      containerStyle,
      fill,
      unfilledColor,
      color,
      borderColor,
    } = style;
    if (type === 'circle') {
      return (
        <View style={containerStyle}>
          <Progress.Circle
            size={size}
            progress={percentage / 100}
            showsText={showsText}
            formatText={formatText}
            indeterminate={indeterminate}
            fill={fill}
            unfilledColor={unfilledColor}
            color={color}
            borderColor={borderColor}
          />
        </View>
      );
    }
  }
}

export default DownloadIndicatorClass;
