import React from 'react';
import { View } from 'react-native';
import { Block, Text, Icon } from 'galio-framework';

class EmptyList extends React.Component {
  render() {
    const { short } = this.props;

    return (
      <Block flex space="around">
        <View style={{ alignItems: 'center' }}>
          <Icon
            size={short ? 48 : 96}
            name="file"
            family="feather"
            color="gray"
          />
          <Text h5 muted>There are no items to show</Text>
          <Text h6 muted>(pull down to refresh)</Text>
        </View>
      </Block>
    );
  }
}

export default EmptyList;
