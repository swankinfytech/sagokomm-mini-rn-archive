import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Platform, Linking, Dimensions, StatusBar } from 'react-native';
import { Icon } from 'galio-framework';
import { MATERIAL_THEME } from 'constants';

// TODO: add iOS app store link

const { width } = Dimensions.get('window');

export class AppMigrate extends Component {
  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center' }}>
        <StatusBar backgroundColor={MATERIAL_THEME.COLORS.FACEBOOK} />
        <View>
          <View style={{ width, height: 64, backgroundColor: MATERIAL_THEME.COLORS.MUTED, marginTop: 32 }} />
          <View style={topSecondarySlant} />
          <View style={{ width, height: 64, backgroundColor: MATERIAL_THEME.COLORS.FACEBOOK, marginTop: -160 }} />
          <View style={topSlant} />
        </View>
        <View>
          <Text style={{ textAlign: 'center', fontSize: 20, color: '#000', marginBottom: 20, alignSelf: 'center', padding: 10, borderRadius: 10 }}>
            This application has been migrated to a different version.
            Please download the new version from the Play Store.
          </Text>
          <View style={{ alignItems: 'center' }}>
            {
              (Platform.OS === 'android') ?
              <TouchableOpacity
                style={{ backgroundColor: '#5E9F22', flexDirection: 'row', padding: 10, borderRadius: 10 }}
                onPress={() => Linking.openURL(this.props.androidLink)}
              >
                <Icon family="FontAwesome" name="android" style={{ fontSize: 30, color: '#fff' }} />
                <Text style={{ alignSelf: 'center', marginLeft: 10, fontSize: 16, color: '#fff' }}>Go to Play Store</Text>
              </TouchableOpacity> :
              <TouchableOpacity
                style={{ backgroundColor: '#000', flexDirection: 'row', padding: 10, borderRadius: 10 }}
                onPress={() => Linking.openURL(this.props.iosLink)}
              >
                <Icon family="AntDesign" name="apple1" style={{ fontSize: 30, color: '#fff' }} />
                <Text style={{ alignSelf: 'center', marginLeft: 10, fontSize: 16, color: '#fff' }}>Go to App Store</Text>
              </TouchableOpacity>
            }
          </View>
        </View>
        <View>
          <View style={bottomSlant} />
          <View style={{ width, height: 32, backgroundColor: MATERIAL_THEME.COLORS.FACEBOOK }} />
        </View>
      </View>
    );
  }
}

const topSlant = {
  width,
  height: 64,
  borderTopWidth: 32,
  borderBottomWidth: 32,
  borderTopColor: MATERIAL_THEME.COLORS.FACEBOOK,
  borderBottomColor: 'rgba(0,0,0,0)',
  borderLeftWidth: width / 2,
  borderRightWidth: width / 2,
  borderLeftColor: MATERIAL_THEME.COLORS.FACEBOOK,
  borderRightColor: 'rgba(0,0,0,0)',
};

const topSecondarySlant = {
  width,
  height: 64,
  borderTopWidth: 32,
  borderBottomWidth: 32,
  borderTopColor: MATERIAL_THEME.COLORS.MUTED,
  borderBottomColor: 'rgba(0,0,0,0)',
  borderLeftWidth: width / 2,
  borderRightWidth: width / 2,
  borderLeftColor: MATERIAL_THEME.COLORS.MUTED,
  borderRightColor: 'rgba(0,0,0,0)',
};

const bottomSlant = {
  width,
  height: 64,
  borderTopWidth: 32,
  borderBottomWidth: 32,
  borderTopColor: 'rgba(0,0,0,0)',
  borderBottomColor: MATERIAL_THEME.COLORS.FACEBOOK,
  borderLeftWidth: width / 2,
  borderRightWidth: width / 2,
  borderLeftColor: 'rgba(0,0,0,0)',
  borderRightColor: MATERIAL_THEME.COLORS.FACEBOOK,
};
