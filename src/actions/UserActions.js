import {
  SET_USER_DATA,
} from 'actions/actionTypes';

export const setUserDataAction = (payload) => {
  return {
    type: SET_USER_DATA,
    payload
  };
};
