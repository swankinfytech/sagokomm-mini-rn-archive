/* eslint max-len: 0 */

// Common Actions
export const LOGIN = 'LOGIN';
export const SET_ORGANIZATION_CODE = 'SET_ORGANIZATION_CODE';
export const LOGOUT = 'LOGOUT';
export const SET_TOKEN = 'SET_TOKEN';
export const SET_USER_DATA = 'SET_USER_DATA';

// Unused
export const LOGIN_UNSUCCESSFUL = 'login_unsuccessful';
export const LOGIN_SUCCESSFUL = 'login_successful';
export const ATTEMPT_LOGIN = 'attempt_login';
export const LOGIN_USER = 'login_user';
export const LOGOUT_USER = 'logout_user';
export const INITIATE_BACKGROUND_APP_LOADING = 'initiate_background_app_loading';
export const BACKGROUND_APP_LOAD_COMPLETE = 'background_app_load_complete';
export const PASSWORD_RESET_SUCCESSFULLY = 'password_reset_successfully';
export const OBSOLETE = 'obsolete';
