import AsyncStorage from 'utilities/AsyncStorageWrapper';
// import OneSignal from 'react-native-onesignal';
import {
  LOGIN,
  SET_TOKEN,
  SET_ORGANIZATION_CODE,
  LOGOUT,
} from 'actions/actionTypes';
import {
  TOKEN_STORAGE_KEY,
  ORGANIZATION_CODE_STORAGE_KEY,
} from 'constants/Values';

export const loginAction = () => {
  return {
    type: LOGIN,
  };
};

export const setTokenAction = (payload) => {
  AsyncStorage.setItem(TOKEN_STORAGE_KEY, payload);
  return {
    type: SET_TOKEN,
    payload
  };
};

export const setOrganizationCodeAction = (payload) => {
  AsyncStorage.setItem(ORGANIZATION_CODE_STORAGE_KEY, payload);
  return {
    type: SET_ORGANIZATION_CODE,
    payload
  };
};

export const logoutAction = (payload) => {
  AsyncStorage.multiRemove([TOKEN_STORAGE_KEY, ORGANIZATION_CODE_STORAGE_KEY]);
  return {
    type: LOGOUT,
    payload,
  };
};
