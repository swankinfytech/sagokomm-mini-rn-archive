import { Platform } from 'react-native';
import { YouTubeStandaloneIOS, YouTubeStandaloneAndroid } from 'react-native-youtube';
import moment from 'moment';
import compareVersions from 'compare-versions';
import { LOG, YOUTUBE_API_KEY, APP_VERSION } from 'constants/Config';

export const daysInMonth = (month, year) => {
  return new Date(year, month, 0).getDate();
};

export const toTitleCase = (phrase) => {
  return phrase
    .toLowerCase()
    .split(' ')
    .map(word => word.charAt(0).toUpperCase() + word.slice(1))
    .join(' ');
};

export const formatDateTime = (dt) => {
  const time = moment(dt);
  return time.format('Do MMMM YYYY, h:mm a');
};

export const formatShortDateTime = (dt) => {
  const time = moment(dt);
  return time.format("D MMM 'YY, h:mm a");
};

export function getVideoThumbnail(url) {
  let id;
  try {
    id = url.split('?v=')[1];
    if (id) {
      id = id.split('&')[0];
    } else {
      id = url.split('.be/')[1];
      id = id.split('?')[0];
    }
  } catch (e) {
    if (LOG) {
      console.log(e);
    }
  }
  return `https://img.youtube.com/vi/${id}/0.jpg`;
}

export function playVideo(url, err) {
  try {
    let id;
    id = url.split('?v=')[1];
    if (id) {
      id = id.split('&')[0];
    } else {
      id = url.split('.be/')[1];
      id = id.split('?')[0];
    }
    if (Platform.OS === 'ios') {
      YouTubeStandaloneIOS.playVideo(id)
      .then(message => console.log(message))
      .catch(errorMessage => err(errorMessage));
    } else {
      YouTubeStandaloneAndroid.playVideo({
        apiKey: YOUTUBE_API_KEY,
        videoId: id,
        autoplay: true,
      })
      .then(() => console.log('Standalone Player Exited'))
      .catch(errorMessage => err(errorMessage));
    }
  } catch (e) {
    err(e);
  }
}

export const stringTruncate = (str, length) => {
  return str.length > length ? str.slice(0, length).concat('...') : str;
};

export const secondsFormatter = (value) => {
  const hours = parseInt(value / 3600, 10) > 0 ? parseInt(value / 3600, 10) + `hr${parseInt(value / 3600, 10) > 1 ? 's' : ''}` : '';
  const hoursRemainder = value % 3600;
  const mins = parseInt(hoursRemainder / 60, 10) > 0 ? ('00' + parseInt(hoursRemainder / 60, 10)).slice(-2) + `min${parseInt(hoursRemainder / 60, 10) > 1 ? 's' : ''}` : '';
  const secs = hoursRemainder % 60 > 0 ? ('00' + (hoursRemainder % 60)).slice(-2) + `sec${hoursRemainder % 60 > 1 ? 's' : ''}` : '';
  let formattedTime = parseInt(value / 3600, 10) > 0 ? hours + ' ' + mins : mins + ' ' + secs;
  formattedTime = formattedTime.trim();
  return formattedTime;
};

export const logError = (error) => {
  if (LOG) {
    console.log(error);
  }
};

export const checkVersionObsolete = (VERSION_CODE) => {
  return compareVersions.compare(VERSION_CODE, APP_VERSION, '>');
};

/**
 * Function to scale Image sizes
 * originalWidth & originalHeight are used to calculate aspect ratio
 * At least one of originalWidth or originalHeight must be provided
 * If both are given, you are an idiot and width is used
 * @param  {int} originalWidth
 * @param  {int} originalHeight
 * @param  {int} scaledWidth
 * @param  {int} scaledHeight
 * @return {object}
 */
export const scaleImageSize = ({
  originalWidth,
  originalHeight,
  scaledWidth,
  scaledHeight,
}) => {
  if (scaledWidth) {
    return {
      scaledWidth,
      scaledHeight: Math.floor((scaledWidth / originalWidth) * originalHeight),
    };
  } else if (scaledHeight) {
    return {
      scaledWidth: Math.floor((scaledHeight / originalHeight) * originalWidth),
      scaledHeight,
    };
  }
  return {
    scaledWidth: originalWidth,
    scaledHeight: originalHeight,
  };
};
