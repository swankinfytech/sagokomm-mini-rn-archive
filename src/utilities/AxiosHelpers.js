import axios from 'axios';
import store from 'reducers';
import {
  LOG,
  ROOT_DOMAIN,
  PROTOCOL,
  DEV_ENV,
  BYPASS_SUBDOMAIN,
} from 'constants/Config';

/**
 * Function to resolve URL to be used for API calls
 * @param  {String} url
 * @param  {Object} [options={}] Extra options to be used, for eg. subdomain, which, if present, will override the subdomain in application state
 * @return {String}
 */
const _getURL = (url, options = {}) => {
  if (options.fqdnPassed) {
    return url;
  }
  const state = store.getState();
  let subdomain = state.Auth.organizationCode;
  if (options.subdomain) {
    subdomain = options.subdomain;
  }
  let apiURL;

  /**
   * Evaluate whether or not to use subdomain to form apiURL
   * If either DEV_ENV or BYPASS_SUBDOMAIN is false, subdomain will be checked
   * Only when both are true, will the if case be bypassed.
   * The second half of if condition is to mitigate effort of logging in again and
   * again when spinning up development enivornment.
   * The second half will always be true when DEV_ENV is false so the BYPASS_SUBDOMAIN
   * matters only in Dev Env. It is useless in PROD env.
   */
  if (subdomain && (!DEV_ENV || !BYPASS_SUBDOMAIN)) {
    apiURL = `${PROTOCOL}://${subdomain}.${ROOT_DOMAIN}/api/v1/${url}`;
  } else {
    apiURL = `${PROTOCOL}://${ROOT_DOMAIN}/api/v1/${url}`;
  }
  if (LOG) { console.log('API URL', apiURL); }
  return apiURL;
};

export const GET = (url, options) => {
  if (LOG) { console.log('GET URL:', url); }
  return new Promise((resolve, reject) => {
    const headers = _createHeaders();
    axios.get(_getURL(url, options), { headers })
    .then(({ data }) => {
      _checkResponse(data);
      if (LOG) { console.log('Success:', data.data); }
      resolve(data.data);
    })
    .catch((error) => {
      _handleError(error, reject);
    });
  });
};

export const POST = (url, body, options = {}) => {
  if (LOG) { console.log('POST URL:', url, ',body: ', body); }
  return new Promise((resolve, reject) => {
    const headers = _createHeaders();
    axios.post(_getURL(url, options), body, { headers })
    .then(({ data }) => {
      _checkResponse(data);
      if (LOG) { console.log('Success:', data.data); }
      resolve(data.data);
    })
    .catch((error) => {
      _handleError(error, reject);
    });
  });
};

export const PUT = (url, body) => {
  if (LOG) { console.log('PUT URL:', url, ',body: ', body); }
  return new Promise((resolve, reject) => {
    const headers = _createHeaders();
    axios.put(_getURL(url), body, { headers })
    .then(({ data }) => {
      _checkResponse(data);
      if (LOG) { console.log('Success:', data.data); }
      resolve(data.data);
    })
    .catch((error) => {
      _handleError(error, reject);
    });
  });
};

export const DELETE = (url) => {
  if (LOG) { console.log('DELETE URL:', url); }
  return new Promise((resolve, reject) => {
    const headers = _createHeaders();
    axios.delete(_getURL(url), { headers })
    .then(({ data }) => {
      _checkResponse(data);
      if (LOG) { console.log('Success:', data.data); }
      resolve(data.data);
    })
    .catch((error) => {
      _handleError(error, reject);
    });
  });
};

export const networkError = (error = {}) => {
  if (error.message && (
    error.message === 'Network Error'
    || error.message === 'Malformed Response'
  )) {
    return true;
  }
  return false;
};

const _createHeaders = () => {
  const state = store.getState();
  const { token } = state.Auth;
  return {
    Authorization: `Bearer ${token}`
  };
};

const _handleError = (error, reject) => {
  if (error.response && error.response.data) {
    if (LOG) { console.log(error.response.data); }
    reject(error.response.data);
    return;
  }
  if (LOG) { console.log(error); }
  reject(error);
};

const _checkResponse = (data) => {
  if (!data || !data.success || data.code !== 200) {
    throw new Error('Malformed Response');
  }
};
