import RNZoomUsBridge, { RNZoomUsBridgeEventEmitter } from '@aayush123/react-native-zoom-us-bridge';
import { NativeEventEmitter } from 'react-native';
import { VALUES, CONFIG } from 'constants';

export const getZoomEventEmitter = () => {
  return new NativeEventEmitter(RNZoomUsBridgeEventEmitter);
};

export const launchConference = ({ conference, user }) => {
  const { provider, provider_data } = conference;
  if (provider === VALUES.CONFERENCE_PROVIDERS.ZOOM) {
    return launchZoomConference({ provider_data, user });
  }
};

export const initializeZoomBridge = () => {
  RNZoomUsBridge.initialize(
    CONFIG.ZOOM_APP_KEY,
    CONFIG.ZOOM_APP_SECRET,
  )
  .then(() => {
    if (CONFIG.LOG) {
      console.log('Zoom Bridge Initialized');
    }
  })
  .catch((e) => {
    if (CONFIG.LOG) {
      console.log('Zoom Bridge Initialization Failed');
      console.log(e);
    }
  });
};

const launchZoomConference = ({ provider_data, user }) => {
  if (CONFIG.LOG) {
    console.log('Launching Zoom Conference');
    console.log(`Conference ID: ${provider_data.conference_id}`);
    console.log(`Conference Password: ${provider_data.conference_password}`);
    console.log(`User Name: ${user.name}`);
  }
  return RNZoomUsBridge.joinMeeting(
    provider_data.conference_id,
    user.name,
    provider_data.conference_password,
    {
      no_driving_mode: true,
      no_invite: true,
      no_text_password: true,
      no_text_meeting_id: true,
    },
  );
};
