module.exports = {
  extends: 'rallycoding',
  settings: {
    'import/resolver': {
      'babel-module': {}
    }
  },
  rules: {
    'max-len': 0,
    'no-unused-vars': [2, {'args': 'after-used', 'argsIgnorePattern': 'props'}],
    'no-shadow': ['error', { 'allow': ['props'] }],
    'no-underscore-dangle': 0,
    'arrow-body-style': 0,
  }
};
